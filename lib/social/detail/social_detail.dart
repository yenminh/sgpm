import 'package:flutter/material.dart';
import 'package:project_aparment/data/thread_social.dart';
import 'package:project_aparment/social/detail/social_detail_bloc.dart';
import 'package:project_aparment/social/socical_page.dart';

class SocialDetailThread extends StatefulWidget {
  final ThreadSocial thread;

  const SocialDetailThread({Key key, this.thread}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SocialDetailState();
}

class _SocialDetailState extends State<SocialDetailThread> {
  bool isPosting = false;
  SocialDetailBloc _bloc;
  int length;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _bloc = SocialDetailBloc(threadSocial: widget.thread);
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    _bloc.dispose();
  }

  Widget item(ThreadSocial thread, int index) => ItemThread(
      isHideLike: true,
      isNoCard: true,
      key: Key(thread.id.toString()),
      thread: thread,
      index: index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(),
      body: Column(
        children: <Widget>[
          ItemThread(
            thread: widget.thread,
            isHideLike: true,
          ),
          Container(
            width: double.infinity,
            child: StreamBuilder<bool>(
              initialData: false,
              stream: _bloc.isHasNextStream,
              builder: (context, snapshot) {
                print("${snapshot.data != null && snapshot.data}");
                return Visibility(
                  visible: snapshot.data != null && snapshot.data,
                  child: InkWell(
                      onTap: () => _bloc.loadPreList(),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 24, right: 16, top: 16, bottom: 16),
                        child: Text("Xem các tin nhắn trước"),
                      )),
                );
              },
            ),
          ),
          Expanded(
            child: StreamBuilder<List<ThreadSocial>>(
              initialData: List(),
              stream: _bloc.threadSocialStream,
              builder: (context, snapshot) {
                return ListView.builder(
                  controller: _scrollController,
                  itemBuilder: (context, index) {
                    if (length != snapshot.data.length) {
                      length = snapshot.data.length;
                      Future.delayed(
                          Duration(milliseconds: 300),
                          () => _scrollController.animateTo(
                              _scrollController.position.maxScrollExtent,
                              curve: Curves.easeIn,
                              duration: Duration(milliseconds: 200)));
                    }
                    return item(snapshot.data[index], index);
                  },
                  itemCount: snapshot.data.length,
                );
              },
            ),
          ),
          SafeArea(
            bottom: true,
            child: BottomReply(
              bloc: _bloc,
            ),
          )
        ],
      ),
    );
  }
}

class BottomReply extends StatefulWidget {
  final SocialDetailBloc bloc;

  const BottomReply({Key key, this.bloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomReplyState();
}

class _BottomReplyState extends State<BottomReply> {
  TextEditingController _controller;
  bool isPosting = false;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Divider(
          height: 1,
        ),
        Padding(
          padding:
              EdgeInsets.only(left: 16.0, right: 8.0, top: 16.0, bottom: 8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  maxLines: 1,
                  controller: _controller,
                  decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context).primaryColor, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      hintText: 'Viết bình luận...'),
                ),
              ),
              Visibility(
                visible: !isPosting,
                child: IconButton(
                    onPressed: () async {
                      setState(() {
                        isPosting = true;
                      });
                      await widget.bloc.postCommentRootRoot(_controller.text);
                      setState(() {
                        _controller.text = "";
                        isPosting = false;
                      });
                    },
                    icon: Icon(
                      Icons.send,
                      color: Theme.of(context).primaryColor,
                    )),
                replacement: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
