import 'dart:async';

import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/data/thread_social.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class SocialDetailBloc extends BlocBase {
  List<ThreadSocial> _listData = List();
  BehaviorSubject<List<ThreadSocial>> _threadSocialBehaviorSubject =
      BehaviorSubject();

  Sink<List<ThreadSocial>> get _threadSocialIn =>
      _threadSocialBehaviorSubject.sink;

  Stream<List<ThreadSocial>> get threadSocialStream =>
      _threadSocialBehaviorSubject.stream;

  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<bool> _isHasNextSubject = BehaviorSubject();

  Sink<bool> get _loadNextIn => _isHasNextSubject.sink;

  Stream<bool> get isHasNextStream => _isHasNextSubject.stream;

  //
  Api _api = Api.instance;
  int pageSize;
  int page = 1;
  int currentPage;
  int lastPage;
  bool isHasNext = true;
  bool isLoadding = false;
  bool isBlockLoadNext = false;
  Timer timer;
  final ThreadSocial threadSocial;

  SocialDetailBloc({this.threadSocial}) {
    timer = Timer.periodic(Duration(milliseconds: 2500), _onTimeCallBack);
    _loadFirstNoPage();
  }

  _loadFirstNoPage() async {
    await loadListNoPage();
    loadLastList(true);
  }

  loadListNoPage() async {
    var res = await _api.loadListMessageReply(page, this.threadSocial);
    var result = ThreadObject.fromJson(res.data);
    pageSize = result.perPage;
    lastPage = result.lastPage;
    currentPage = result.currentPage;
  }

  loadLastList(bool progressing) async {
    if (progressing) _stateIn.add(STATE_LOADING);
    var res = await _api.loadListMessageReply(lastPage, this.threadSocial);
    var result = ThreadObject.fromJson(res.data);
    pageSize = result.perPage;
    lastPage = result.lastPage;
    currentPage = result.currentPage;
    if(!isBlockLoadNext){
      isHasNext = result.prevPageUrl != null;
      _loadNextIn.add(isHasNext);
    }
    if (result.data != null && result.data.isNotEmpty) {
      if (progressing) {
        _listData.addAll(result.data);
      } else {
        if (_listData != null && _listData.isNotEmpty) {
          int positionBreak = -1;
          for (int i = result.data.length - 1; i >= 0; i--) {
            if (result.data[i].id == _listData.last.id &&
                i != result.data.length - 1) {
              positionBreak = i;
              break;
            }
          }
          if (positionBreak != -1) {
            _listData.addAll(result.data.sublist(positionBreak + 1));
          }
        } else {
          _listData = result.data;
        }
      }
    }
    _threadSocialIn.add(_listData);
    if (progressing) _stateIn.add(STATE_SUCCESS);
  }

  loadPreList() async {
    var res =
        await _api.loadListMessageReply(currentPage - 1, this.threadSocial);
    var result = ThreadObject.fromJson(res.data);
    pageSize = result.perPage;
    lastPage = result.lastPage;
    currentPage = result.currentPage;
    isHasNext = result.prevPageUrl != null && result.prevPageUrl.isNotEmpty;
    if(!isHasNext) isBlockLoadNext = true;
    _loadNextIn.add(isHasNext);

    if (result.data != null && result.data.isNotEmpty) {
      for (int i = 0; i < result.data.length; i++) {
        _listData.insert(i, result.data[i]);
      }
      _threadSocialIn.add(_listData);
    }
  }

  postCommentRootRoot(String content) async {
    var res = await _api.postCommentRootRoot(content, this.threadSocial);
    /*if (res = true) {
      loadListMessage(page);
    }*/
  }

  like(ThreadSocial threadSocial) async {
    _api.like(threadSocial.id);
  }

  @override
  void dispose() {
    timer.cancel();
    _threadSocialBehaviorSubject.close();
    _stateBehaviorSubject.close();
    _isHasNextSubject.close();
  }

  void _onTimeCallBack(Timer timer) {
    loadLastList(false);
  }
}
