import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project_aparment/data/thread_social.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/social/detail/social_detail.dart';
import 'package:project_aparment/social/sosical_bloc.dart';
import 'dart:math' as Math;

class SocialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _SocialFul();
  }
}

class _SocialFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SocialState();
}

class _SocialState extends State<_SocialFul> {
  SocialBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = SocialBloc();
  }

  Widget item(ThreadSocial thread, int index) => InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SocialDetailThread(
                        thread: thread,
                      )));
        },
        child: ItemThread(
            key: Key(thread.id.toString()), thread: thread, index: index),
      );

  @override
  Widget build(BuildContext context) {
    Widget appBar = AppBar(
      title: Text("Cộng đồng"),
    );
    return BlocProvider(
      bloc: _bloc,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          appBar,
          Expanded(
            child: StreamBuilder<int>(
              initialData: STATE_INIT,
              stream: _bloc.stateStream,
              builder: (context, snapshot) {
                if (snapshot.data == STATE_LOADING)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                return StreamBuilder<List<ThreadSocial>>(
                  initialData: List(),
                  stream: _bloc.threadSocialStream,
                  builder: (context, snapshot) {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        if (index == snapshot.data.length - 1 &&
                            _bloc.isHasNext) {
                          _bloc.loadListMessage(_bloc.page + 1);
                        }
                        return item(snapshot.data[index], index);
                      },
                      itemCount: snapshot.data.length,
                    );
                  },
                );
              },
            ),
          ),
          BottomReply(
            bloc: _bloc,
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
  }
}

class BottomReply extends StatefulWidget {
  final SocialBloc bloc;

  const BottomReply({Key key, this.bloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomReplyState();
}

class _BottomReplyState extends State<BottomReply> {
  TextEditingController _controller;
  bool isPosting = false;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Divider(
          height: 1,
        ),
        Padding(
          padding:
              EdgeInsets.only(left: 16.0, right: 8.0, top: 16.0, bottom: 8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  maxLines: 1,
                  controller: _controller,
                  decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context).primaryColor, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      hintText: 'Viết bình luận...'),
                ),
              ),
              Visibility(
                visible: !isPosting,
                child: IconButton(
                    onPressed: () async {
                      setState(() {
                        isPosting = true;
                      });
                      await widget.bloc.postMessage(_controller.text);
                      setState(() {
                        _controller.text = "";
                        isPosting = false;
                      });
                    },
                    icon: Icon(
                      Icons.send,
                      color: Theme.of(context).primaryColor,
                    )),
                replacement: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class ItemThread extends StatefulWidget {
  final ThreadSocial thread;
  final int index;
  final bool isNoCard;
  final bool isHideLike;

  const ItemThread(
      {Key key, this.thread, this.index, this.isNoCard, this.isHideLike})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ItemThreadState(thread, index);
}

class _ItemThreadState extends State<ItemThread> {
  ThreadSocial thread;
  int index;

  _ItemThreadState(this.thread, this.index);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      key: Key(thread.id.toString()),
      margin: (widget.isNoCard != null && widget.isNoCard)
          ? EdgeInsets.only(left: 24.0)
          : (index != null && index == 0)
              ? EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0)
              : EdgeInsets.all(
                  8.0,
                ),
      elevation: (widget.isNoCard != null && widget.isNoCard) ? 0 : 2,
      child: Container(
        padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: NetworkImage((thread.resident != null)
                      ? thread.resident.photoUrl
                      : ""),
                  backgroundColor: Colors.grey,
                ),
                Container(
                  width: 16.0,
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        thread.resident != null
                            ? thread.resident.ten
                            : (thread.user != null ? thread.user.ten : ""),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).primaryColor),
                      ),
                      Container(
                        height: 4,
                      ),
                      Text(
                        DateFormat("dd-MM-yyy hh:mm:ss").format(
                            DateTime.fromMillisecondsSinceEpoch(
                                thread.time * 1000)),
                        style: Theme.of(context).textTheme.caption,
                      )
                    ],
                  ),
                )
              ],
            ),
            Container(
              height: 16.0,
            ),
            Text(
              thread.content,
              style: Theme.of(context).textTheme.body1,
            ),
            widget.isHideLike != null && widget.isHideLike
                ? Container(
                    height: 16.0,
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton.icon(
                          splashColor: Colors.transparent,
                          onPressed: () async {
                            bool res =
                                await BlocProvider.of<SocialBloc>(context)
                                    .like(thread);
                            setState(() {
                              print(
                                  "thread.thichCount ${thread.thichCount} res $res");
                              thread.thichCount = res
                                  ? thread.thichCount + 1
                                  : Math.max(thread.thichCount - 1, 0);
                              print("thread.thichCount ${thread.thichCount}");
                            });
                          },
                          icon: Icon(
                            Icons.favorite,
                            color: Colors.red[200],
                          ),
                          label: Text("${thread.thichCount}")),
                      thread.hasChildMessageCount != null
                          ? FlatButton.icon(
                              splashColor: Colors.transparent,
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            SocialDetailThread(
                                              thread: thread,
                                            )));
                              },
                              icon: Icon(
                                Icons.comment,
                                color: Colors.red[200],
                              ),
                              label:
                                  Text("${thread.hasChildMessageCount ?? 0}"))
                          : Container()
                    ],
                  )
          ],
        ),
      ),
    );
  }
}
