import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/data/thread_social.dart';
import 'package:project_aparment/main.dart';

class SocialBloc extends BlocBase {
  List<ThreadSocial> _listData = List();
  BehaviorSubject<List<ThreadSocial>> _threadSocialBehaviorSubject =
      BehaviorSubject();

  Sink<List<ThreadSocial>> get _threadSocialIn =>
      _threadSocialBehaviorSubject.sink;

  Stream<List<ThreadSocial>> get threadSocialStream =>
      _threadSocialBehaviorSubject.stream;

  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  Api _api = Api.instance;
  int pageSize;
  int page = 1;
  bool isHasNext = true;
  bool isLoadding = false;
  Timer timer;

  SocialBloc() {
    loadListMessage(page);
    timer = Timer.periodic(Duration(milliseconds: 2500), _onTimeCallBack);
  }

  loadListMessage(int page) async {
    if (isHasNext) {
      if (!isLoadding) {
        isLoadding = true;
        if (page == 1) _stateIn.add(STATE_LOADING);
        var res = await _api.loadListMessage(page);
        var result = ThreadObject.fromJson(res.data);
        pageSize = result.perPage;
        isHasNext = result.data.length >= pageSize;
        //result.data.map((item) => ThreadSocial.fromJson(Map.from(item))).toList();
        if (page == 1)
          _listData = result.data;
        else
          _listData.addAll(result.data);
        _threadSocialIn.add(_listData);
        if (page == 1) _stateIn.add(STATE_SUCCESS);
        isLoadding = false;
      }
    }
  }

  loadListMessageNoState(int page) async {
    var res = await _api.loadListMessage(page);
    var result = ThreadObject.fromJson(res.data);
    pageSize = result.perPage;
    isHasNext = result.data.length >= pageSize;
    if (_listData != null && _listData.isNotEmpty) {
      if (result.data != null && result.data.isNotEmpty) {
        int positionBreak = -1;
        for (int i = 0; i < result.data.length; i++) {
          if (result.data[i].id == _listData[0].id) {
            positionBreak = i;
            break;
          }
        }
        if (positionBreak > 0) {
          for (int i = 0; i < positionBreak; i++) {
            _listData.insert(i, result.data[i]);
          }
          _threadSocialIn.add(_listData);
        }
      }
    } else {
      _listData = result.data;
      _threadSocialIn.add(_listData);
    }
  }

  postMessage(String content) async {
    var res = await _api.postCommentRoot(content);
    /*if (res = true) {
      loadListMessage(page);
    }*/
  }

  Future<bool> like(ThreadSocial threadSocial) async {
    var res = await _api.like(threadSocial.id);
    print("res != null && res.data != null ${res != null && res.data != null}");
    if (res != null && res.data != null) {
      print("res.data.thich ${res.data.thich}");
      if (res.data.thich != 0) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  @override
  void dispose() {
    timer.cancel();
    _threadSocialBehaviorSubject.close();
    _stateBehaviorSubject.close();
  }

  void _onTimeCallBack(Timer timer) {
    loadListMessageNoState(1);
  }
}
