class BookServiceReponse {
  String soluong;
  String idProvider;
  String ngay;
  int idChungcu;
  int idBlock;
  int idCanho;
  String mota;
  int dongia;
  int httt;
  int status;
  int ngaytt;
  int ngaytao;
  int ngaysua;
  String photo;
  int id;

  BookServiceReponse(
      {this.soluong,
        this.idProvider,
        this.ngay,
        this.idChungcu,
        this.idBlock,
        this.idCanho,
        this.mota,
        this.dongia,
        this.httt,
        this.status,
        this.ngaytt,
        this.ngaytao,
        this.ngaysua,
        this.photo,
        this.id});

  BookServiceReponse.fromJson(Map<String, dynamic> json) {
    soluong = json['soluong'];
    idProvider = json['id_provider'];
    ngay = json['ngay'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    idCanho = json['id_canho'];
    mota = json['mota'];
    dongia = json['dongia'];
    httt = json['httt'];
    status = json['status'];
    ngaytt = json['ngaytt'];
    ngaytao = json['ngaytao'];
    ngaysua = json['ngaysua'];
    photo = json['photo'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['soluong'] = this.soluong;
    data['id_provider'] = this.idProvider;
    data['ngay'] = this.ngay;
    data['id_chungcu'] = this.idChungcu;
    data['id_block'] = this.idBlock;
    data['id_canho'] = this.idCanho;
    data['mota'] = this.mota;
    data['dongia'] = this.dongia;
    data['httt'] = this.httt;
    data['status'] = this.status;
    data['ngaytt'] = this.ngaytt;
    data['ngaytao'] = this.ngaytao;
    data['ngaysua'] = this.ngaysua;
    data['photo'] = this.photo;
    data['id'] = this.id;
    return data;
  }
}