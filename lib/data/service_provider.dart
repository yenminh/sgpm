class ServiceProvider {
  int id;
  int idServicie;
  String ten;
  String mota;
  String photo;
  int gia;
  String donvi;
  String imageUrl;

  ServiceProvider({this.id, this.idServicie, this.ten, this.mota, this.photo, this.gia, this.donvi, this.imageUrl});

  factory ServiceProvider.fromJson(Map<String, dynamic> json) {
    print(json.toString());
    return ServiceProvider(
      id: json["id"],
      idServicie: json["id_services"],
      ten: json["ten"],
      mota: json["mota"],
      photo: json["photo"],
      gia: json["gia"],
      donvi: json["donvi"],
      imageUrl: json["imageUrl"],
    );
  }
}
