import 'package:project_aparment/data/apartment.dart';
import 'package:project_aparment/data/block.dart';
import 'package:project_aparment/data/building.dart';

const String KEY_USER = "user_key";

class User {
  int id;
  int idChungCu;
  int idBlock;
  int idCanHo;
  int loai;
  String ho;
  String ten;
  int ngaySinh;
  String email;
  String dienThoai;
  String cmnd;
  String photo;
  String diaChi;
  String mota;
  String photoUrl;
  Building building;
  Block block;
  Apartment apartment;

  User(
      {this.id,
      this.idChungCu,
      this.idBlock,
      this.idCanHo,
      this.loai,
      this.ho,
      this.ten,
      this.ngaySinh,
      this.email,
      this.dienThoai,
      this.cmnd,
      this.photo,
      this.diaChi,
      this.mota,
      this.photoUrl,
      this.block,
      this.building,
      this.apartment});

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        idChungCu: json["id_chungcu"],
        idBlock: json["id_block"],
        idCanHo: json["id_canho"],
        loai: json["loai"],
        ho: json["ho"],
        ten: json["ten"],
        ngaySinh: json["ngaysinh"],
        email: json["email"],
        dienThoai: json["dienthoai"],
        cmnd: json["cmnd"],
        photo: json["photo"],
        diaChi: json["diachi"],
        mota: json["mota"],
        photoUrl: json["photoUrl"],
        block: Block.fromJson(Map.from(json["block"])),
        apartment: Apartment.fromJson(Map.from(json["canho"])),
        building: Building.fromJson(Map.from(json["chungcu"])),
      );
}
