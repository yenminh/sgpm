class Block {
  int id;
  int idChungcu;
  String code;
  String ma;
  String ten;
  String mota;
  int ngaytao;
  int ngaysua;

  Block(
      {this.id,
        this.idChungcu,
        this.code,
        this.ma,
        this.ten,
        this.mota,
        this.ngaytao,
        this.ngaysua});

  Block.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    code = json['code'];
    ma = json['ma'];
    ten = json['ten'];
    mota = json['mota'];
    ngaytao = json['ngaytao'];
    ngaysua = json['ngaysua'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_chungcu'] = this.idChungcu;
    data['code'] = this.code;
    data['ma'] = this.ma;
    data['ten'] = this.ten;
    data['mota'] = this.mota;
    data['ngaytao'] = this.ngaytao;
    data['ngaysua'] = this.ngaysua;
    return data;
  }
}