class ServiceDetail {
  /*
   "id": 1,
            "ten": "Dịch vụ nước",
            "mota": "",
            "photo": "16179671.png"
   */
  int id;
  int idGroup;
  String ten;
  String mota;
  String photo;
  String photoUrl;

  ServiceDetail({this.id,this.idGroup, this.ten, this.mota, this.photo, this.photoUrl});

  factory ServiceDetail.fromJson(Map<String, dynamic> json) {
    print(json.toString());
    return ServiceDetail(
      id: json["id"],
      idGroup: json["id_group"],
      ten: json["ten"],
      mota: json["mota"],
      photo: json["photo"],
      photoUrl: json["photoUrl"]
    );
  }
}
