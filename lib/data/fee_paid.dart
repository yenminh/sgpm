class FeePaid {
  int id;
  int idChungcu;
  int idBlock;
  int idCanho;
  int idCudan;
  int status;
  String ghichu;
  String logs;
  int ngaytt;
  int ngaytao;
  int idDonhang;
  String loaiphi;
  int idPhi;
  int giatien;
  String tenPhi;
  Phidichvu phidichvu;

  FeePaid(
      {this.id,
        this.idChungcu,
        this.idBlock,
        this.idCanho,
        this.idCudan,
        this.status,
        this.ghichu,
        this.logs,
        this.ngaytt,
        this.ngaytao,
        this.idDonhang,
        this.loaiphi,
        this.idPhi,
        this.giatien,
        this.tenPhi,
        this.phidichvu});

  FeePaid.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    idCanho = json['id_canho'];
    idCudan = json['id_cudan'];
    status = json['status'];
    ghichu = json['ghichu'];
    logs = json['logs'];
    ngaytt = json['ngaytt'];
    ngaytao = json['ngaytao'];
    idDonhang = json['id_donhang'];
    loaiphi = json['loaiphi'];
    idPhi = json['id_phi'];
    giatien = json['giatien'];
    tenPhi = json['ten_phi'];
    phidichvu = json['phidichvu'] != null
        ? new Phidichvu.fromJson(json['phidichvu'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_chungcu'] = this.idChungcu;
    data['id_block'] = this.idBlock;
    data['id_canho'] = this.idCanho;
    data['id_cudan'] = this.idCudan;
    data['status'] = this.status;
    data['ghichu'] = this.ghichu;
    data['logs'] = this.logs;
    data['ngaytt'] = this.ngaytt;
    data['ngaytao'] = this.ngaytao;
    data['id_donhang'] = this.idDonhang;
    data['loaiphi'] = this.loaiphi;
    data['id_phi'] = this.idPhi;
    data['giatien'] = this.giatien;
    data['ten_phi'] = this.tenPhi;
    if (this.phidichvu != null) {
      data['phidichvu'] = this.phidichvu.toJson();
    }
    return data;
  }
}

class Phidichvu {
  int id;
  int idGroup;
  String ten;
  String mota;
  String photo;

  Phidichvu({this.id, this.idGroup, this.ten, this.mota, this.photo});

  Phidichvu.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idGroup = json['id_group'];
    ten = json['ten'];
    mota = json['mota'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_group'] = this.idGroup;
    data['ten'] = this.ten;
    data['mota'] = this.mota;
    data['photo'] = this.photo;
    return data;
  }
}