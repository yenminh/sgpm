import 'package:project_aparment/data/canho.dart';
import 'package:project_aparment/data/fee.dart';
import 'package:project_aparment/data/fee_order.dart';
import 'package:project_aparment/data/fee_paid.dart';
import 'package:project_aparment/data/like_response.dart';
import 'package:project_aparment/data/list_response.dart';
import 'package:project_aparment/data/login.dart';
import 'package:project_aparment/data/news.dart';
import 'package:project_aparment/data/notification.dart';
import 'package:project_aparment/data/service.dart';
import 'package:project_aparment/data/service_provider.dart';
import 'package:project_aparment/data/thread_social.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/data/vehicle_model.dart';

class BaseResponse<T> {
  int codeToken;
  int dataType;
  int status;
  T data;
  String message;
  String errors;

  BaseResponse(
      {this.codeToken,
      this.dataType,
      this.status,
      this.data,
      this.message,
      this.errors});

  factory BaseResponse.fromJson(Map<String, dynamic> json, {Type type}) {
    var data;
    if (T == LoginResponse) {
      data = LoginResponse.fromJson(json["data"]);
    } else if (T == NewsListResponse) {
      data = NewsListResponse.fromJson(json["data"]);
    } else if (T == User) {
      data = User.fromJson(json["data"]);
    } else if (T == ListResponse<HomeNotification>().runtimeType) {
      if (type != null) {
        if (type == HomeNotification) {
          data = ListResponse<HomeNotification>.fromJson(json["data"]);
        }
      }
    } else if (type == ServiceProvider) {
      if (type != null) {
        if (type == ServiceProvider) {
          data = json["data"];
        }
      }
    } else if (type == Apartment) {
      data = json["data"];
    } else if (type == Fee) {
      data = json["data"];
    }
    if (type == ThreadObject) {
      data = json["data"];
    } else if (type == FeePaid) {
      data = (List.from(json["data"]["data"])
              .map((item) => FeePaid.fromJson(Map<String, dynamic>.from(item))))
          .toList();
    } else if (type == FeeOrder) {
      data = FeeOrder.fromJson(json["data"]);
    } else if (type == Service) {
      if (type != null) {
        if (type == Service) {
          data = json["data"];
        }
      }
    } else if (T == VehicleResponseModel) {
      data = VehicleResponseModel.fromJson(json["data"]);
    }
    if (T == LikeResponse) {
      data = LikeResponse.fromJson(Map.from(json["data"]));
    }
    return BaseResponse(
        codeToken: json["code_token"],
        dataType: json["datatype"],
        status: json["status"],
        message: json["msg"],
        errors: json["errors"],
        data: data);
  }
}
