class HomeNotification {
  int id;
  int idChungcu;
  int idBlock;
  int loai;
  String tenVi;
  String tenEn;
  String slugVi;
  String slugEn;
  String motaVi;
  String motaEn;
  String noidungVi;
  String noidungEn;
  int stt;
  int hienthi;
  int dateFrom;
  int dateTo;
  int ngaytao;
  int ngaysua;
  int views;
  int idUser;
  String photo500;
  String photoFull;

  HomeNotification(
      {this.id,
        this.idChungcu,
        this.idBlock,
        this.loai,
        this.tenVi,
        this.tenEn,
        this.slugVi,
        this.slugEn,
        this.motaVi,
        this.motaEn,
        this.noidungVi,
        this.noidungEn,
        this.stt,
        this.hienthi,
        this.dateFrom,
        this.dateTo,
        this.ngaytao,
        this.ngaysua,
        this.views,
        this.idUser,
        this.photo500,
        this.photoFull});

  HomeNotification.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    loai = json['loai'];
    tenVi = json['ten_vi'];
    tenEn = json['ten_en'];
    slugVi = json['slug_vi'];
    slugEn = json['slug_en'];
    motaVi = json['mota_vi'];
    motaEn = json['mota_en'];
    noidungVi = json['noidung_vi'];
    noidungEn = json['noidung_en'];
    stt = json['stt'];
    hienthi = json['hienthi'];
    dateFrom = json['date_from'];
    dateTo = json['date_to'];
    ngaytao = json['ngaytao'];
    ngaysua = json['ngaysua'];
    views = json['views'];
    idUser = json['id_user'];
    photo500 = json['photo_500'];
    photoFull = json['photo_full'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_chungcu'] = this.idChungcu;
    data['id_block'] = this.idBlock;
    data['loai'] = this.loai;
    data['ten_vi'] = this.tenVi;
    data['ten_en'] = this.tenEn;
    data['slug_vi'] = this.slugVi;
    data['slug_en'] = this.slugEn;
    data['mota_vi'] = this.motaVi;
    data['mota_en'] = this.motaEn;
    data['noidung_vi'] = this.noidungVi;
    data['noidung_en'] = this.noidungEn;
    data['stt'] = this.stt;
    data['hienthi'] = this.hienthi;
    data['date_from'] = this.dateFrom;
    data['date_to'] = this.dateTo;
    data['ngaytao'] = this.ngaytao;
    data['ngaysua'] = this.ngaysua;
    data['views'] = this.views;
    data['id_user'] = this.idUser;
    data['photo_500'] = this.photo500;
    data['photo_full'] = this.photoFull;
    return data;
  }
}
