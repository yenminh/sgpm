class Building {
  int id;
  String code;
  String ten;
  String email;
  String diachi;
  String gioithieu;
  String banquanly;
  String banquantri;
  String hotline1;
  String hotline2;
  String photo;
  String photo2;
  String photo3;
  String logo;
  int phiquanly;
  int phidien;
  int phinuoc;
  int phigas;
  int phiguixe;
  int phikhac;
  int ngaytao;
  int ngaysua;

  Building(
      {this.id,
        this.code,
        this.ten,
        this.email,
        this.diachi,
        this.gioithieu,
        this.banquanly,
        this.banquantri,
        this.hotline1,
        this.hotline2,
        this.photo,
        this.photo2,
        this.photo3,
        this.logo,
        this.phiquanly,
        this.phidien,
        this.phinuoc,
        this.phigas,
        this.phiguixe,
        this.phikhac,
        this.ngaytao,
        this.ngaysua});

  Building.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    ten = json['ten'];
    email = json['email'];
    diachi = json['diachi'];
    gioithieu = json['gioithieu'];
    banquanly = json['banquanly'];
    banquantri = json['banquantri'];
    hotline1 = json['hotline1'];
    hotline2 = json['hotline2'];
    photo = json['photo'];
    photo2 = json['photo2'];
    photo3 = json['photo3'];
    logo = json['logo'];
    phiquanly = json['phiquanly'];
    phidien = json['phidien'];
    phinuoc = json['phinuoc'];
    phigas = json['phigas'];
    phiguixe = json['phiguixe'];
    phikhac = json['phikhac'];
    ngaytao = json['ngaytao'];
    ngaysua = json['ngaysua'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['ten'] = this.ten;
    data['email'] = this.email;
    data['diachi'] = this.diachi;
    data['gioithieu'] = this.gioithieu;
    data['banquanly'] = this.banquanly;
    data['banquantri'] = this.banquantri;
    data['hotline1'] = this.hotline1;
    data['hotline2'] = this.hotline2;
    data['photo'] = this.photo;
    data['photo2'] = this.photo2;
    data['photo3'] = this.photo3;
    data['logo'] = this.logo;
    data['phiquanly'] = this.phiquanly;
    data['phidien'] = this.phidien;
    data['phinuoc'] = this.phinuoc;
    data['phigas'] = this.phigas;
    data['phiguixe'] = this.phiguixe;
    data['phikhac'] = this.phikhac;
    data['ngaytao'] = this.ngaytao;
    data['ngaysua'] = this.ngaysua;
    return data;
  }
}