class Apartment {
  int id;
  int idChungcu;
  int idBlock;
  String code;
  String ma;
  int loai;
  String so;
  int dientich;
  int phiquanly;
  String mota;

  Apartment(
      {this.id,
        this.idChungcu,
        this.idBlock,
        this.code,
        this.ma,
        this.loai,
        this.so,
        this.dientich,
        this.phiquanly,
        this.mota});

  Apartment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    code = json['code'];
    ma = json['ma'];
    loai = json['loai'];
    so = json['so'];
    dientich = json['dientich'];
    phiquanly = json['phiquanly'];
    mota = json['mota'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_chungcu'] = this.idChungcu;
    data['id_block'] = this.idBlock;
    data['code'] = this.code;
    data['ma'] = this.ma;
    data['loai'] = this.loai;
    data['so'] = this.so;
    data['dientich'] = this.dientich;
    data['phiquanly'] = this.phiquanly;
    data['mota'] = this.mota;
    return data;
  }
}