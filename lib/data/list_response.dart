import 'package:project_aparment/data/news.dart';
import 'package:project_aparment/data/notification.dart';
import 'package:project_aparment/data/service_provider.dart';

class ListResponse<T> {
  int currentPage;
  int from;
  int lastPage;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;
  List<T> dataList;

  ListResponse(
      {this.currentPage,
      this.from,
      this.lastPage,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total,
      this.dataList});

  factory ListResponse.fromJson(Map<String, dynamic> json) {
    var data = List();
    switch (T) {
      case News:
        {
          data = (json["data"] as List).map((item) => News.fromJson(item as Map<String, dynamic>)).toList();
        }
        break;
      case HomeNotification:
        {
          data = (json["data"] as List).map((item) => HomeNotification.fromJson(item as Map<String, dynamic>)).toList();
        }
        break;
      case ServiceProvider:
        {
          data = (json["data"] as List).map((item) => ServiceProvider.fromJson(item as Map<String, dynamic>)).toList();
        }
        break;
    }
    return ListResponse(
        currentPage: json["current_page"],
        from: json["from"],
        lastPage: json["last_page"],
        nextPageUrl: json["next_page_url"],
        path: json["path"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
        dataList: data);
  }
}
