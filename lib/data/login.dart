import 'package:project_aparment/data/user_login.dart';

class LoginResponse {
  String token;
  UserLogin userLogin;
  List<String> subscribeTopics;

  LoginResponse({this.token, this.userLogin, this.subscribeTopics});

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      token: json["token"],
      userLogin: UserLogin.fromJson(json["profile"]),
      subscribeTopics: List<String>.from(json["subscribe_topics"]),
    );
  }
}
