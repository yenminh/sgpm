import 'dart:convert';

import 'package:crypto/crypto.dart';

class BillData {
  String billCode;
  String command = "PAYMENT";
  String merchantCode = "GCENVISCO";
  String orderId;
  String returnUrl = "http://chungcu.sgpm.com.vn/api/public/qlcc/GetResult.php";
  int transAmount;
  String version = "2.0";
  String checkSum;
  String description = "";

  BillData(
      {this.billCode,
      this.command = "PAYMENT",
      this.merchantCode = "GCENVISCO",
      this.orderId,
      this.returnUrl =
          "http://chungcu.sgpm.com.vn/api/public/qlcc/GetResult.php",
      this.transAmount,
      this.version = "2.0",
      this.checkSum,
      this.description = ""});

  String getUrlCTT() {
    var checkSum = getChecksum();
    return "https://sandbox.viettel.vn/PaymentGateway/payment?version=2.0&command=PAYMENT&merchant_code=$merchantCode&order_id=$orderId&trans_amount=$transAmount&locale=Vi&billcode=$orderId&check_sum=$checkSum";
  }

  String getChecksum() {
    var accessCode =
        "d41d8cd98f00b204e9800998ecf8427eddfa60793019f0251db5f8362eea1e62";

    var key = utf8.encode(
        "d41d8cd98f00b204e9800998ecf8427e8fc2ea5f3583f0c950bfe2d04b485c16");
    var checkSumRaw =
        "$accessCode$billCode$command$merchantCode$orderId$transAmount${"2.0"}";
    var sumSha1Raw = Hmac(sha1, key).convert(utf8.encode(checkSumRaw));
    return base64Encode(sumSha1Raw.bytes)
        .replaceAll("=", "%3D"); //.Base64Encoder().convert(sumSha1Raw.bytes);
  }
}
