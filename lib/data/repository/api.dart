import 'dart:async';
import 'dart:convert' as json;
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:async/async.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;
import 'package:project_aparment/data/base_response.dart';
import 'package:project_aparment/data/book_service_reponse.dart';
import 'package:project_aparment/data/canho.dart';
import 'package:project_aparment/data/fee.dart';
import 'package:project_aparment/data/fee_order.dart';
import 'package:project_aparment/data/fee_paid.dart';
import 'package:project_aparment/data/like_response.dart';
import 'package:project_aparment/data/list_response.dart';
import 'package:project_aparment/data/login.dart';
import 'package:project_aparment/data/news.dart';
import 'package:project_aparment/data/service.dart';
import 'package:project_aparment/data/service_provider.dart';
import 'package:project_aparment/data/thread_social.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/data/notification.dart';
import 'package:project_aparment/data/vehicle_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

const KEY_TOKEN = "token_key";
const url = "http://api.sgpm.vn/api/v1/";

const loginPath = url + "auth/login";
const logoutPath = url + "auth/logout";
const newsPath = url + "news";
const userProfilePath = url + "users/me";
const notificationsPath = url + "notifications";
const loadServiceProvidersPath = url + "services/";
const loadServicePath = url + "group-services";
const loadCanHoPath = url + "users/canho";
const loadServiceDetailPath = url + "group-services";
const postServiceBooking = url + "services/";
const loadFeePath = url + "list-fee";
const loadFeePaidPath = url + "order-payment";
const loadMessagePath = url + "messages";
const upaLoadAvatarPath = url + "users/avatar";
const orderPayment = url + "order-payment";
const feedBack = url + "user/gopy";
const changePassPath = url + "users/change-password";
const vehiclePath = url + "users/xe";

class UserAgentClient extends http.BaseClient {
  final http.Client _inner;

  UserAgentClient(this._inner);

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    // TODO: implement send
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(KEY_TOKEN);
    if (token != null && token.isNotEmpty) {
      request.headers['Authorization'] = "Bearer$token";
    }
    return _inner.send(request);
  }

  @override
  Future<http.Response> post(url,
      {Map<String, String> headers, body, json.Encoding encoding}) async {
    // TODO: implement post
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(KEY_TOKEN);
    if (token != null && token.isNotEmpty) {
      headers['Authorization'] = "Bearer$token";
    }
    return _inner.post(url, headers: headers);
  }

  @override
  Future<http.Response> get(url, {Map<String, String> headers}) async {
    // TODO: implement get
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(KEY_TOKEN);
    if (token != null && token.isNotEmpty) {
      headers['Authorization'] = "Bearer $token";
    }
    return _inner.get(url, headers: headers);
  }
}

class Api {
  Api._privateConstructor();

  static final Api _instance = Api._privateConstructor();

  static Api get instance {
    return _instance;
  }

  Future<BaseResponse<NewsListResponse>> loadNewsPage({int page}) async {
    var header = await loadHeaders();
    var uri = Uri.parse(newsPath);
    uri = uri.replace(queryParameters: {"page": page.toString()});
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<NewsListResponse>.fromJson(body);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<NewsListResponse>> loadNotePage({int page}) async {
    var header = await loadHeaders();
    var uri = Uri.parse(newsPath);
    uri = uri.replace(queryParameters: {"sotaycudan": 1.toString()});
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      print(body.toString());
      return BaseResponse<NewsListResponse>.fromJson(body);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<User>> loadUser() async {
    var header = await loadHeaders();
    var response = await http.get(userProfilePath, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<User>.fromJson(body);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<ListResponse<HomeNotification>>> loadNotification(
      {int page}) async {
    var header = await loadHeaders();
    var uri = Uri.parse(notificationsPath);
    uri = uri.replace(queryParameters: {"page": page.toString()});
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<ListResponse<HomeNotification>>.fromJson(body,
          type: HomeNotification);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<HomeNotification>> loadNotificationDetail(
      {int id}) async {
    var header = await loadHeaders();
    var uri = Uri.parse(notificationsPath + "/$id");
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<HomeNotification>.fromJson(body,
          type: HomeNotification);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<List<dynamic>>> loadListServiceProvider(int id) async {
    var header = await loadHeaders();
    var uri = Uri.parse(loadServiceProvidersPath + id.toString() + "/provider");
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<List<dynamic>>.fromJson(body, type: ServiceProvider);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<Map<String, dynamic>>> loadCanHo() async {
    var header = await loadHeaders();
    var uri = Uri.parse(loadCanHoPath);
    var response = await http.get(loadCanHoPath, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      print((response.body));
      return BaseResponse<Map<String, dynamic>>.fromJson(body, type: Apartment);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<List<dynamic>>> loadListService() async {
    var header = await loadHeaders();
    var uri = Uri.parse(loadServicePath);
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      print("body: $body");
      return BaseResponse<List<dynamic>>.fromJson(body, type: Service);
    } else {
      print("body: ${response.body}");
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<List<dynamic>>> loadListServiceDetail(String id) async {
    var header = await loadHeaders();
    var uri = Uri.parse("$loadServiceDetailPath/$id");
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<List<dynamic>>.fromJson(body, type: Service);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<Map<String, dynamic>>> loadListMessage(int page) async {
    var header = await loadHeaders();
    var uri = Uri.parse("$loadMessagePath");
    uri = uri.replace(queryParameters: {"page": page.toString()});
    print(uri.toString());
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<Map<String, dynamic>>.fromJson(body,
          type: ThreadObject);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<Map<String, dynamic>>> loadListMessageReply(
      int page, ThreadSocial thread) async {
    var header = await loadHeaders();
    var uri = Uri.parse("$loadMessagePath/${thread.id}/replies");
    uri = uri.replace(queryParameters: {"page": page.toString()});
    print(uri.toString());
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<Map<String, dynamic>>.fromJson(body,
          type: ThreadObject);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<http.StreamedResponse> bookSv(
      {int idService,
      String number,
      DateTime time,
      String description,
      File image,
      int provider}) async {
    var request = new http.MultipartRequest(
      "POST",
      Uri.parse("$postServiceBooking$idService"),
    );
    request.headers.addAll(await loadHeaders());
    request.fields['soluong'] = number != null ? number.toString() : "";
    request.fields['ngay'] = time.millisecond.toString();
    request.fields['mota'] = description != null ? description : "";
    request.fields['id_provider'] = provider != null ? provider.toString() : "";
    if (image != null) {
      var stream = http.ByteStream(DelegatingStream.typed(image.openRead()));
      var length = await image.length();
      var multipartFile = new http.MultipartFile('file', stream, length,
          filename: basename(image.path));
      // add file to multipart
      request.files.add(multipartFile);
    }
    return request.send();
  }

  Future<http.StreamedResponse> uploadAvatar({
    File image,
  }) async {
    var request = new http.MultipartRequest(
      "POST",
      Uri.parse("$upaLoadAvatarPath"),
    );
    request.headers.addAll(await loadHeaders());
    if (image != null) {
      var stream = http.ByteStream(DelegatingStream.typed(image.openRead()));
      var length = await image.length();
      var multipartFile = new http.MultipartFile('photo', stream, length,
          filename: basename(image.path));
      // add file to multipart
      request.files.add(multipartFile);
    }
    return request.send();
  }

  Future<BaseResponse<List<dynamic>>> getListFee() async {
    var header = await loadHeaders();
    var uri = Uri.parse("$loadFeePath");
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<List<dynamic>>.fromJson(body, type: Fee);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<List<FeePaid>>> getListFeePaid() async {
    var header = await loadHeaders();
    var uri = Uri.parse("$loadFeePaidPath");
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<List<FeePaid>>.fromJson(body, type: FeePaid);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<FeeOrder>> getFee(List<Fee> listFee) async {
    var header = await loadHeaders();
    var uri = Uri.parse("$orderPayment");
    var body = JsonEncoder()
        .convert({"listfee": listFee.map((item) => item.toJson()).toList()});
    var response = await http.post(uri, headers: header, body: body);
    if (response.statusCode == 200) {
      print(response.body.toString());
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<FeeOrder>.fromJson(body, type: FeeOrder);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<VehicleResponseModel>> loadListVehicle(int page) async {
    var header = await loadHeaders();
    var uri = Uri.parse("$vehiclePath");
    uri = uri.replace(queryParameters: {"page": page.toString()});
    print(uri.toString());
    var response = await http.get(uri, headers: header);
    if (response.statusCode == 200) {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<VehicleResponseModel>.fromJson(body,
          type: VehicleResponseModel);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<LoginResponse>> login(
      {String userName, String password, String token}) async {
    var body = JsonEncoder().convert(
        {"email": userName, "password": password, "device_token": token});
    var response = await http.post(Uri.parse(loginPath), body: body, headers: {
      "Accept": "application/json",
      "content-type": "application/json"
    });
    if (response.statusCode == 200) {
      print("reponse: ${response.body}.");
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<LoginResponse>.fromJson(body);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<BaseResponse<LikeResponse>> like(int id) async {
    print("$id");
    var header = await loadHeaders();
    var response = await http.post(Uri.parse(loadMessagePath + "/$id/like"),
        headers: header);
    if (response.statusCode == 200) {
      print("reponse: ${response.body}.");
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return BaseResponse<LikeResponse>.fromJson(body);
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<bool> postCommentRoot(String content) async {
    var header = await loadHeaders();
    var body = JsonEncoder().convert({"content": content});
    var response = await http.post(Uri.parse(loadMessagePath),
        body: body, headers: header);
    if (response.statusCode == 200) {
      print("reponse: ${response.body}.");
      return true;
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<bool> postCommentRootRoot(
      String content, ThreadSocial threadSocial) async {
    var header = await loadHeaders();
    var body = JsonEncoder()
        .convert({"content": content, "parent_id": threadSocial.id});
    var response = await http.post(Uri.parse(loadMessagePath),
        body: body, headers: header);
    if (response.statusCode == 200) {
      print("reponse: ${response.body}.");
      return true;
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<bool> postFeedBack(String title, String content) async {
    var header = await loadHeaders();
    var body = JsonEncoder().convert({"tieude": title, "noidung": content});
    var response =
        await http.post(Uri.parse(feedBack), body: body, headers: header);
    if (response.statusCode == 200) {
      print("reponse: ${response.body}.");
      return true;
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<bool> postCommentChild(int id, String content) async {
    var header = await loadHeaders();
    var body = JsonEncoder().convert({"content": content, "parent_id": id});
    var response = await http.post(Uri.parse(loadMessagePath),
        body: body, headers: header);
    if (response.statusCode == 200) {
      //var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      return true;
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<bool> logOut(String deviceToken, String loginToken) async {
    var header = await loadHeaders();
    var body = JsonEncoder()
        .convert({"device_token": deviceToken, "login_token": loginToken});
    var response =
        await http.post(Uri.parse(logoutPath), body: body, headers: header);
    if (response.statusCode == 200) {
      print("logout success");
      return true;
    } else {
      var body = json.jsonDecode(response.body) as Map<String, dynamic>;
      throw Exception(body['message']);
    }
  }

  Future<Map<String, String>> loadHeaderForm() async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(KEY_TOKEN);
    return {
      "Accept": "application/json",
      "content-type": "multipart/form-data",
      'Authorization': "Bearer $token"
    };
  }

  Future<Map<String, String>> loadHeaders() async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(KEY_TOKEN);
    return {
      "Accept": "application/json",
      "content-type": "application/json",
      'Authorization': "Bearer $token"
    };
  }
}
