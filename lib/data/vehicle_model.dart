class VehicleModel {
  int id;
  int idChungcu;
  int idBlock;
  int idCanho;
  int idCudan;
  int loaixe;
  String bienso;
  String noidangky;
  String sokhung;
  String somay;
  int giatien;
  int dateFrom;
  int dateTo;
  String log;

  VehicleModel(
      {this.id,
        this.idChungcu,
        this.idBlock,
        this.idCanho,
        this.idCudan,
        this.loaixe,
        this.bienso,
        this.noidangky,
        this.sokhung,
        this.somay,
        this.giatien,
        this.dateFrom,
        this.dateTo,
        this.log});

  VehicleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    idCanho = json['id_canho'];
    idCudan = json['id_cudan'];
    loaixe = json['loaixe'];
    bienso = json['bienso'];
    noidangky = json['noidangky'];
    sokhung = json['sokhung'];
    somay = json['somay'];
    giatien = json['giatien'];
    dateFrom = json['date_from'];
    dateTo = json['date_to'];
    log = json['log'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_chungcu'] = this.idChungcu;
    data['id_block'] = this.idBlock;
    data['id_canho'] = this.idCanho;
    data['id_cudan'] = this.idCudan;
    data['loaixe'] = this.loaixe;
    data['bienso'] = this.bienso;
    data['noidangky'] = this.noidangky;
    data['sokhung'] = this.sokhung;
    data['somay'] = this.somay;
    data['giatien'] = this.giatien;
    data['date_from'] = this.dateFrom;
    data['date_to'] = this.dateTo;
    data['log'] = this.log;
    return data;
  }
}
class VehicleResponseModel {
  int currentPage;
  List<VehicleModel> data;
  int from;
  int lastPage;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  VehicleResponseModel(
      {this.currentPage,
        this.data,
        this.from,
        this.lastPage,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  VehicleResponseModel.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<VehicleModel>();
      json['data'].forEach((v) {
        data.add(new VehicleModel.fromJson(v));
      });
    }
    from = json['from'];
    lastPage = json['last_page'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}