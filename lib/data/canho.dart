import 'package:project_aparment/data/user.dart';

class Apartment {
  int id;
  int idChungcu;
  int idBlock;
  String code;
  String ma;
  int loai;
  String so;
  int dientich;
  int phiquanly;
  String mota;
  List<User> cudan;

  Apartment({this.id,
    this.idChungcu,
    this.idBlock,
    this.code,
    this.loai,
    this.so,
    this.dientich,
    this.phiquanly,
    this.mota,
    this.cudan});

  Apartment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    code = json['code'];
    ma = json['ma'];
    loai = json['loai'];
    so = json['so'];
    dientich = json['dientich'];
    phiquanly = json['phiquanly'];
    mota = json['mota'];
    if (json['cudan'] != null) {
      cudan = new List<User>();
      json['cudan'].forEach((v) {
        cudan.add( User.fromJson(v));
      });
    }
  }

}
