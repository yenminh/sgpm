class UserLogin {
  int id;
  String userName;
  String email;
  String ten;
  int sex;
  int role;
  int idChungcu;
  int hienThi;
  String randomKey;
  String com;

  UserLogin(
      {this.id,
      this.userName,
      this.email,
      this.ten,
      this.sex,
      this.role,
      this.idChungcu,
      this.hienThi,
      this.randomKey,
      this.com});

  factory UserLogin.fromJson(Map<String, dynamic> json) {
    return UserLogin(
      id: json["id"],
      userName: json["username"],
      email: json["email"],
      ten: json["ten"],
      sex: json["sex"],
      role: json["role"],
      idChungcu: json["id_chungcu"],
      hienThi: json["hienthi"],
      randomKey: json["randomkey"],
      com: json["com"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": this.id,
        "username": this.userName,
        "email": this.email,
        "ten": this.ten,
        "role": this.role,
        "id_chungcu": this.idChungcu,
        "hienthi": this.hienThi,
        "randomekey": this.randomKey,
        "com": this.com
      };
}
