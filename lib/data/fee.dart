class Fee {
  int id;
  int phatsinh;
  int ngaytao;
  String tenchiphi;
  String loaiphi;
  String ngay;

  Fee(
      {this.id,
        this.phatsinh,
        this.ngaytao,
        this.tenchiphi,
        this.loaiphi,
        this.ngay});

  Fee.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    phatsinh = json['phatsinh'];
    ngaytao = json['ngaytao'];
    tenchiphi = json['tenchiphi'];
    loaiphi = json['loaiphi'];
    ngay = json['ngay'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['phatsinh'] = this.phatsinh;
    data['ngaytao'] = this.ngaytao;
    data['tenchiphi'] = this.tenchiphi;
    data['loaiphi'] = this.loaiphi;
    data['ngay'] = this.ngay;
    return data;
  }
}