class LikeResponse {
  int id;
  int idChungcu;
  int idBlock;
  int idCanho;
  int idGroup;
  int idCudan;
  int idUser;
  int idParent;
  String content;
  String photo;
  int thich;
  int thichCount;
  int active;
  int time;
  String imageUrl;

  LikeResponse(
      {this.id,
        this.idChungcu,
        this.idBlock,
        this.idCanho,
        this.idGroup,
        this.idCudan,
        this.idUser,
        this.idParent,
        this.content,
        this.photo,
        this.thich,
        this.thichCount,
        this.active,
        this.time,
        this.imageUrl});

  LikeResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    idCanho = json['id_canho'];
    idGroup = json['id_group'];
    idCudan = json['id_cudan'];
    idUser = json['id_user'];
    idParent = json['id_parent'];
    content = json['content'];
    photo = json['photo'];
    thich = json['thich'];
    thichCount = json['thich_count'];
    active = json['active'];
    time = json['time'];
    imageUrl = json['imageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_chungcu'] = this.idChungcu;
    data['id_block'] = this.idBlock;
    data['id_canho'] = this.idCanho;
    data['id_group'] = this.idGroup;
    data['id_cudan'] = this.idCudan;
    data['id_user'] = this.idUser;
    data['id_parent'] = this.idParent;
    data['content'] = this.content;
    data['photo'] = this.photo;
    data['thich'] = this.thich;
    data['thich_count'] = this.thichCount;
    data['active'] = this.active;
    data['time'] = this.time;
    data['imageUrl'] = this.imageUrl;
    return data;
  }
}