class FeeOrder {
  int orderId;
  int amount;

  FeeOrder({this.orderId, this.amount});

  FeeOrder.fromJson(Map<String, dynamic> json) {
    orderId = json['order_id'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_id'] = this.orderId;
    data['amount'] = this.amount;
    return data;
  }
}