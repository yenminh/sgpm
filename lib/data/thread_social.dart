import 'package:project_aparment/data/user.dart';

class ThreadSocial {
  int id;
  int idChungcu;
  int idBlock;
  int idCanho;
  int idGroup;
  int idCudan;
  int idUser;
  int idParent;
  String content;
  String photo;
  int thich;
  int thichCount;
  int active;
  int time;
  int hasChildMessageCount;
  User user;
  Resident resident;

  ThreadSocial(
      {this.id,
      this.idChungcu,
      this.idBlock,
      this.idCanho,
      this.idGroup,
      this.idCudan,
      this.idUser,
      this.idParent,
      this.content,
      this.photo,
      this.thich,
      this.thichCount,
      this.active,
      this.time,
      this.hasChildMessageCount,
      this.user,
      this.resident});

  ThreadSocial.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    idCanho = json['id_canho'];
    idGroup = json['id_group'];
    idCudan = json['id_cudan'];
    idUser = json['id_user'];
    idParent = json['id_parent'];
    content = json['content'];
    photo = json['photo'];
    thich = json['thich'];
    thichCount = json['thich_count'];
    active = json['active'];
    time = json['time'];
    hasChildMessageCount = json['has_child_message_count'];
    user = json['user'] != null ? User.fromJson(Map.from(json['user'])): null;
    resident = json['resident'] != null ? new Resident.fromJson(json['resident']) : null;
  }

}

class Resident {
  int id;
  int idChungcu;
  int idBlock;
  int idCanho;
  int loai;
  String ho;
  String ten;
  int ngaysinh;
  String email;
  String dienthoai;
  String cmnd;
  String photo;
  String diachi;
  String mota;
  String photoUrl;

  Resident(
      {this.id,
        this.idChungcu,
        this.idBlock,
        this.idCanho,
        this.loai,
        this.ho,
        this.ten,
        this.ngaysinh,
        this.email,
        this.dienthoai,
        this.cmnd,
        this.photo,
        this.diachi,
        this.mota,
        this.photoUrl});

  Resident.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChungcu = json['id_chungcu'];
    idBlock = json['id_block'];
    idCanho = json['id_canho'];
    loai = json['loai'];
    ho = json['ho'];
    ten = json['ten'];
    ngaysinh = json['ngaysinh'];
    email = json['email'];
    dienthoai = json['dienthoai'];
    cmnd = json['cmnd'];
    photo = json['photo'];
    diachi = json['diachi'];
    mota = json['mota'];
    photoUrl = json['photoUrl'];
  }
}

class ThreadObject {
  int currentPage;
  List<ThreadSocial> data;
  int from;
  int lastPage;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  ThreadObject(
      {this.currentPage,
      this.data,
      this.from,
      this.lastPage,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  ThreadObject.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<ThreadSocial>();
      json['data'].forEach((v) {
        data.add(new ThreadSocial.fromJson(v));
      });
    }
    from = json['from'];
    lastPage = json['last_page'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }
}
