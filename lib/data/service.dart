class Service {
  /*
   "id": 1,
            "ten": "Dịch vụ nước",
            "mota": "",
            "photo": "16179671.png"
   */
  int id;
  String ten;
  String mota;
  String photo;
  String photoUrl;

  Service({this.id, this.ten, this.mota, this.photo, this.photoUrl});

  factory Service.fromJson(Map<String, dynamic> json) {
    return Service(
      id: json["id"],
      ten: json["ten"],
      mota: json["mota"],
      photo: json["photo"],
      photoUrl: json["photoUrl"]
    );
  }
}
