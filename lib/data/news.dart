class NewsListResponse {
  int currentPage;
  int from;
  int lastPage;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;
  List<News> dataList;

  NewsListResponse(
      {this.currentPage,
      this.from,
      this.lastPage,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total,
      this.dataList});

  factory NewsListResponse.fromJson(Map<String, dynamic> json) {
    return NewsListResponse(
        currentPage: json["current_page"],
        from: json["from"],
        lastPage: json["last_page"],
        nextPageUrl: json["next_page_url"],
        path: json["path"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
        dataList: (json["data"] as List).map((item) => News.fromJson(item as Map<String, dynamic>)).toList()
    );
  }
}

class News {
  int id;
  int idChungCu;
  String link;
  int idItem;
  int idCat;
  int tinNoiBat;
  int tinHot;
  String tenVi;
  String tenEn;
  String tenKhongDau;
  String slugVi;
  String slugEn;
  String title;
  String keyWords;
  String description;
  String motaVi;
  String motaEn;
  String youtube;
  String thoiLuong;
  String noiDungVi;
  String noiDungEn;
  String tag;
  String loaiTin;
  String photo;
  String thumb;
  int stt;
  int hienThi;
  int private;
  int hasVideo;
  int hasPhoto;
  int dateFrom;
  int dateTo;
  int ngayTao;
  int ngaySua;
  int views;
  int luotXem;
  int idUser;
  String photo500;
  String photoFull;

  News(
      {this.id,
      this.idChungCu,
      this.link,
      this.idItem,
      this.idCat,
      this.tinNoiBat,
      this.tinHot,
      this.tenVi,
      this.tenEn,
      this.tenKhongDau,
      this.slugVi,
      this.slugEn,
      this.title,
      this.keyWords,
      this.description,
      this.motaVi,
      this.motaEn,
      this.youtube,
      this.thoiLuong,
      this.noiDungVi,
      this.noiDungEn,
      this.tag,
      this.loaiTin,
      this.photo,
      this.thumb,
      this.stt,
      this.hienThi,
      this.private,
      this.hasVideo,
      this.hasPhoto,
      this.dateFrom,
      this.dateTo,
      this.ngayTao,
      this.ngaySua,
      this.views,
      this.luotXem,
      this.idUser,
      this.photo500,
      this.photoFull});

  factory News.fromJson(Map<String, dynamic> json) => News(
      id: json["id"],
      idChungCu: json["id_chungcu"],
      link: json["link"],
      idItem: json["id_item"],
      idCat: json["id_cat"],
      tinNoiBat: json["tinnoibat"],
      tinHot: json["tinhot"],
      tenVi: json["ten_vi"],
      tenEn: json["ten_en"],
      tenKhongDau: json["tenkhongdau"],
      slugVi: json["slug_vi"],
      slugEn: json["slug_en"],
      title: json["title"],
      keyWords: json["keywords"],
      description: json["description"],
      motaVi: json["mota_vi"],
      motaEn: json["mota_en"],
      youtube: json["youtube"],
      thoiLuong: json["thoiluong"],
      noiDungVi: json["noidung_vi"],
      noiDungEn: json["noidung_en"],
      tag: json["tag"],
      loaiTin: json["loaitin"],
      photo: json["photo"],
      thumb: json["thumb"],
      stt: json["stt"],
      hienThi: json["hienthi"],
      private: json["private"],
      hasVideo: json["hasVideo"],
      hasPhoto: json["hasPhoto"],
      dateFrom: json["date_from"],
      dateTo: json["date_to"],
      ngayTao: json["ngaytao"],
      ngaySua: json["ngaysua"],
      views: json["views"],
      luotXem: json["luotxem"],
      idUser: json["id_user"],
      photo500: json["photo_500"],
      photoFull: json["photo_full"]);
}
