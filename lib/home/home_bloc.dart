import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc extends BlocBase {
  int currentPage = 0;
  BehaviorSubject<int> _currentPageBehavior = BehaviorSubject();

  Sink<int> get currentPageIn => _currentPageBehavior.sink;

  Stream<int> get currentPageStream => _currentPageBehavior.stream;

  HomeBloc() {
    currentPageStream.listen((page) => currentPage = page);
  }

  @override
  void dispose() {
    _currentPageBehavior.close();
  }
}
