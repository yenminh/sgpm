import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:project_aparment/bill/bill_not_paid_bloc.dart';
import 'package:project_aparment/bill/bill_page.dart';
import 'package:project_aparment/bill/bill_paid_bloc.dart';
import 'package:project_aparment/home/home_bloc.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/notification/news/news_bloc.dart';
import 'package:project_aparment/notification/notification/notifycation_bloc.dart';
import 'package:project_aparment/notification/notification_page.dart';
import 'package:project_aparment/profile/profile_bloc.dart';
import 'package:project_aparment/profile/profile_page.dart';
import 'package:project_aparment/service/service_page.dart';
import 'package:project_aparment/social/socical_page.dart';

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}

class Home extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MyHomePage(title: 'Apparment');
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  HomeBloc _bloc;
  NewsBloc newsBloc;
  NotificationBloc notificationBloc;
  BillNotPaidBloc billNotPaidBloc;
  BillPaidBloc billPadBloc;
  ProfileBloc profileBloc;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  List<Widget> listPage() => [
        BlocProvider(
          bloc: notificationBloc,
          child: BlocProvider(
            bloc: newsBloc,
            child: NotifyRootPage(),
          ),
        ),
        BlocProvider(
          bloc: profileBloc,
          child: ServicePage(),
        ),
        BlocProvider(
          child: BlocProvider<BillPaidBloc>(
              bloc: billPadBloc,
              child: BlocProvider<BillNotPaidBloc>(
                child: BillPage(),
                bloc: billNotPaidBloc,
              )),
        ),
        SocialPage(),
        BlocProvider(
          bloc: profileBloc,
          child: ProfilePage(),
        )
      ];

  //final PageController _pageController = PageController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = HomeBloc();
    newsBloc = NewsBloc();
    notificationBloc = NotificationBloc();
    billNotPaidBloc = BillNotPaidBloc();
    billPadBloc = BillPaidBloc();
    profileBloc = ProfileBloc();
    _configureFbMessage();
  }

  _configureFbMessage() async {
    if (Platform.isIOS) {
      _firebaseMessaging.requestNotificationPermissions();
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print("onMessage: $message");
          _showNotificationLocal(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          print("onLaunch: $message");
          _showNotificationLocal(message);
        },
        onResume: (Map<String, dynamic> message) async {
          print("onResume: $message");
          _showNotificationLocal(message);
        },
      );
    } else {
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print("onMessage: $message");
          _showNotificationLocal(message);
        },
        onBackgroundMessage: myBackgroundMessageHandler,
        onLaunch: (Map<String, dynamic> message) async {
          print("onLaunch: $message");
          _showNotificationLocal(message);
        },
        onResume: (Map<String, dynamic> message) async {
          print("onResume: $message");
          _showNotificationLocal(message);
        },
      );
    }
  }

  _showNotificationLocal(Map<String, dynamic> messageNotification) {
    var data = messageNotification["notification"] ??
        messageNotification["aps"]["alert"];
    String title = data["title"];
    String message = data["body"];
    print("show flush bar");
    Flushbar(
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      icon: SizedBox(
        width: 24,
        height: 24,
        child: Image.asset("images/app_logo.png"),
      ),
      titleText: Text(
        title ?? "",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
          color: Colors.black,
        ),
      ),
      messageText: Text(
        message ?? "",
        style: TextStyle(
          fontSize: 18.0,
          color: Colors.black,
        ),
      ),
      backgroundColor: Colors.white,
      duration: Duration(seconds: 2),
      flushbarPosition: FlushbarPosition.TOP,
    )..show(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget bodyMain = StreamBuilder<int>(
      stream: _bloc.currentPageStream,
      initialData: 0,
      builder: (context, snapshot) => listPage()[snapshot.data],
    );
    return Scaffold(
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: bodyMain,
      ),
      bottomNavigationBar: bottomNavigation(),
    );
  }

  Widget bottomNavigation() => StreamBuilder<int>(
        initialData: _bloc.currentPage,
        stream: _bloc.currentPageStream,
        builder: (context, snapshot) {
          return BottomNavigationBar(
            currentIndex: snapshot.data,
            backgroundColor: Theme.of(context).primaryColor,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Colors.white,
            unselectedItemColor: Colors.white70,
            onTap: _onBottomBarTapped,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.notifications), title: Text("Thông báo")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.business_center), title: Text("Dịch vụ")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.monetization_on), title: Text("Hóa đơn")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.group), title: Text("Cộng đồng")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_circle), title: Text("Tài khoản"))
            ],
          );
        },
      );

  void _onBottomBarTapped(int index) {
    _bloc.currentPageIn.add(index);
  }
}
