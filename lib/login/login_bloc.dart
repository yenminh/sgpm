import 'dart:async';
import 'dart:convert' as Json;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:project_aparment/data/repository/api.dart';

class LoginBloc extends BlocBase {
  String errorMessage = "";
  String token;
  var _userName = "";
  var _password = "";
  final _apiInvalidMessage = 'Invalid Username or Password';
  final _apiYourAccountIsInactive = 'Your account is inactive';
  var _validatorUserNamePass = false;
  var _validatorPasswordPass = false;
  var _isFirsTimeValidate = true;
  BehaviorSubject<String> userNameController = BehaviorSubject();

  BehaviorSubject<int> stateController = BehaviorSubject();
  BehaviorSubject<String> passWordController = BehaviorSubject();
  BehaviorSubject loginController = BehaviorSubject();
  BehaviorSubject<bool> isAuthenticatedController = BehaviorSubject();
  PublishSubject<String> errorMessagePublisher = PublishSubject();

  StreamSink<String> get userNameIn => userNameController.sink;

  StreamSink<String> get _errorMessageIn => errorMessagePublisher.sink;

  Stream<String> get errorMessageStream => errorMessagePublisher.stream;

  StreamSink<String> get passIn => passWordController.sink;

  Stream<String> get userNameErrorStream =>
      userNameController.stream.map((userName) => validateUserName(userName));

  Stream<String> get passErrorStream =>
      passWordController.stream.map((password) => validateUserPass(password));

  Stream<bool> get isAuthenticatedStream => isAuthenticatedController.stream;

  Stream<int> get stateStream => stateController.stream;

  LoginBloc() {
    loginController.stream.listen(handleLogin);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    userNameController.close();
    passWordController.close();
    isAuthenticatedController.close();
    loginController.close();
    errorMessagePublisher.close();
  }

  void handleLogin(data) {
    if (_isFirsTimeValidate) {
      _isFirsTimeValidate = false;
      userNameIn.add(_userName);
      passIn.add(_password);
      validateUserName(_userName);
      validateUserPass(_password);
    }
    if (_validatorPasswordPass && _validatorUserNamePass) {
      _login(userName: this._userName, password: this._password);
    }
  }

  Future<void> _login({String userName, String password}) async {
    stateController.add(STATE_LOADING);
    Api.instance
        .login(userName: userName, password: password, token: token)
        .then((result) {
      if (result != null && result.status == 200) {
        SharedPreferences.getInstance().then((pref) {
          pref.setString(KEY_TOKEN, result.data.token);
          pref.setString(
              KEY_USER, Json.jsonEncode(result.data.userLogin.toJson()));
          result?.data?.subscribeTopics?.forEach((item) {
            FirebaseMessaging().subscribeToTopic(item);
          });
          stateController.add(STATE_SUCCESS);
          isAuthenticatedController.add(true);
        });
        print("topic ${result.data.subscribeTopics}");
      } else {
        print("bloc ${result.message}");
        isAuthenticatedController.add(false);
        errorMessage = result.message;
        _errorMessageIn.add(errorMessage);
        stateController.add(STATE_INIT);
      }
    }).catchError((error) {
      errorMessage = error.toString().replaceAll("Exception: ", "");
      _errorMessageIn.add(errorMessage);
      stateController.add(STATE_INIT);
    });
  }

  validateUserName(String username) {
    this._userName = username;
    if (_isFirsTimeValidate) return null;
    //print("ttt $_userName");
    if (username.isEmpty) {
      _validatorUserNamePass = false;
      return 'Tên đăng nhập không được để trống';
    } else if (username == _apiInvalidMessage) {
      _validatorUserNamePass = false;
      return 'Tên đăng nhập hoặc mật khẩu không đúng.';
    } else if (username == _apiYourAccountIsInactive) {
      _validatorUserNamePass = false;
      return 'Tài khoản chưa được kích hoạt hoặc vô hiệu hóa';
    } else {
      _validatorUserNamePass = true;
      return null;
    }
  }

  validateUserPass(String password) {
    this._password = password;
    if (_isFirsTimeValidate) return null;
    if (password.isEmpty) {
      _validatorPasswordPass = false;
      return 'Mật khẩu không được để trống';
    } else if (password == _apiInvalidMessage) {
      _validatorPasswordPass = false;
      return 'Tên đăng nhập hoặc mật khẩu không đúng.';
    } else {
      _validatorPasswordPass = true;
      return null;
    }
  }

  handleError(Exception error) {
    /* print(error.message);
    if (error.message == _apiInvalidMessage) {
      userNameIn.add(error.message);
      passIn.add(error.message);
    } else {
      userNameIn.add(error.message);
    }*/
  }

  handleResult(result) {
    if (result != null) {
      isAuthenticatedController.sink.add(true);
    }
  }
}
