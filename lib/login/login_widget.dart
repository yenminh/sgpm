import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:project_aparment/login/login_bloc.dart';
import 'package:project_aparment/main.dart';

class LoginPage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LoginPageFul(title: 'Login');
  }
}

class LoginPageFul extends StatefulWidget {
  LoginPageFul({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPageFul> {
  bool _obscureText = true;
  bool _hasFocus = false;
  bool textFieldsIsInFocus = false;
  bool visibleButtonLogin = true;
  bool isFirsStart = true;
  FocusNode userNameFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  LoginBloc _bloc;
  final userNameController = TextEditingController();
  final passWordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _bloc = LoginBloc();
    FirebaseMessaging().getToken().then((value) {
      _bloc.token = value;
      print(value);
    });
    userNameController
        .addListener(() => _bloc.userNameIn.add(userNameController.text));
    passWordController.addListener(
        () => _bloc.passWordController.add(passWordController.text));
    userNameFocusNode.addListener(_onFocusUserNameChange);
    passwordFocusNode.addListener(_onFocusChange);
  }

  Widget buildUserNameText() => Container(
      child: StreamBuilder<String>(
        initialData: null,
        stream: _bloc.userNameErrorStream,
        builder: (context, snapshot) {
          return TextField(
            focusNode: userNameFocusNode,
            controller: userNameController,
            maxLines: 1,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                hintText: 'Nhập địa chỉ email',
                labelText: 'Tài khoản',
                errorText: snapshot.data,
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(25, 25, 25, 0.32)))),
          );
        },
      ),
      margin: const EdgeInsets.only(left: 16.0, right: 16.0));

  Widget buildPassText() => Container(
        child: StreamBuilder<String>(
            initialData: null,
            stream: _bloc.passErrorStream,
            builder: (context, snapshot) {
              return TextFormField(
                  controller: passWordController,
                  maxLines: 1,
                  focusNode: passwordFocusNode,
                  decoration: InputDecoration(
                      hintText: 'Nhập password',
                      labelText: 'Mật khẩu',
                      errorText: snapshot.data,
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(25, 25, 25, 0.32))),
                      suffixIcon: IconButton(
                        icon: Icon(Icons.remove_red_eye),
                        onPressed: () {
                          _obscureText = !_obscureText;
                          if (!_hasFocus) {
                            FocusScope.of(context)
                                .requestFocus(passwordFocusNode);
                          } else {
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          }
                        },
                      )),
                  obscureText: _obscureText);
            }),
        margin: const EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
      );

  Widget buildLoginButton() => Container(
        width: double.infinity,
        child: RaisedButton(
            padding: const EdgeInsets.all(16.0),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)),
            child: Text(
              'Đăng nhập',
              style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w500),
            ),
            textColor: Colors.white,
            color: Theme.of(context).primaryColor,
            splashColor: Colors.white70,
            onPressed: () {
              _bloc.loginController.add(null);
              //_bloc.loginController.add(null);
            }),
        margin: const EdgeInsets.only(
            left: 16.0, right: 16.0, top: 24.0, bottom: 24.0),
      );

  void _onFocusUserNameChange() {
    textFieldsIsInFocus = userNameFocusNode.hasFocus;
  }

  void _onFocusChange() {
    setState(() {
      textFieldsIsInFocus = passwordFocusNode.hasFocus;
      _hasFocus = passwordFocusNode.hasFocus;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _bloc.isAuthenticatedStream.listen((isLogged) {
      Navigator.pushReplacementNamed(context, NAVIGATOR_HOME);
    });
    Widget mainWidget = Stack(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Spacer(
                        flex: 1,
                      ),
                      Image.asset('images/app_logo.png'),
                      Spacer(
                        flex: 1,
                      )
                    ],
                  ),
                ),
                Expanded(
                    flex: 3,
                    child: Column(
                      children: <Widget>[
                        buildUserNameText(),
                        buildPassText(),
                        buildLoginButton(),
                        /*Container(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Quên mật khẩu?",
                                style: TextStyle(
                                  fontSize: 18.0,
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(4.0),
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Text(" Click here",
                                    style: TextStyle(
                                        fontSize: 18.0, color: Color(0xff1D89F5), decoration: TextDecoration.underline)),
                              )
                            ],
                          ),
                        )*/
                      ],
                    ))
              ],
            ),
          ),
        ),
        Positioned.fill(
          child: StreamBuilder(
            initialData: STATE_INIT,
            stream: _bloc.stateStream,
            builder: (context, snapshot) {
              var visibility = snapshot.data == STATE_LOADING ? true : false;
              return Visibility(
                visible: visibility,
                child: Container(
                  color: Colors.white,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: mainWidget,
    );
  }
}
