import 'dart:io';

import 'package:http/http.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class ProfileEditBloc extends BlocBase {
  Api _api = Api.instance;
  User user;
  //subscribe user
  BehaviorSubject<User> _userBehavior = BehaviorSubject();

  Sink<User> get userIn => _userBehavior.sink;

  Stream<User> get userStream => _userBehavior.stream;
  Future<StreamedResponse> updateLoadImage(File image) async {
    return _api.uploadAvatar(image: image);
  }

  Future<void> loadUserInfo() async {
    await _api.loadUser().then((result) {
      if (result != null) {
        user = result.data;
        userIn.add(user);
      }
    }).catchError((ex) {
      print(ex.toString());
    });
  }
  @override
  void dispose() {
    _userBehavior.close();
  }
}
