import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/profile/edit/profile_edit_bloc.dart';

class ProfileEditPage extends StatelessWidget {
  final User user;

  const ProfileEditPage({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _ProfileFul(
      user: user,
    );
  }
}

class _ProfileFul extends StatefulWidget {
  final User user;

  _ProfileFul({Key key, this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfileState();
  }
}

class _ProfileState extends State<_ProfileFul> {
  File image;

  Widget topView() => Container(
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(),
            ),
            Expanded(
                flex: 3,
                child: AspectRatio(
                  aspectRatio: 1.0,
                  child: Stack(
                    children: <Widget>[
                      Positioned.fill(
                        child: StreamBuilder<User>(
                          builder: (context, snapshot) {
                            return CircleAvatar(
                              backgroundColor: Colors.grey,
                              backgroundImage: CachedNetworkImageProvider(
                                _bloc.user != null
                                    ? _bloc.user.photoUrl
                                    : (widget.user != null)
                                        ? widget.user.photoUrl
                                        : "",
                              ),
                            );
                          },
                          stream: _bloc.userStream,
                        ),
                      ),
                      Positioned(
                        right: 0,
                        bottom: 4.0,
                        child: FloatingActionButton(
                            backgroundColor: Colors.white,
                            mini: true,
                            onPressed: () async {
                              await showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      content: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                            width: 100,
                                            height: 100,
                                            child: Column(
                                              children: <Widget>[
                                                IconButton(
                                                  onPressed: () async {
                                                    image = await ImagePicker
                                                        .pickImage(
                                                      source:
                                                          ImageSource.camera,
                                                    );
                                                    Navigator.pop(context);
                                                  },
                                                  icon: Icon(Icons.camera_alt),
                                                ),
                                                Text("Camera")
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: 100,
                                            height: 100,
                                            child: Column(
                                              children: <Widget>[
                                                IconButton(
                                                  onPressed: () async {
                                                    image = await ImagePicker
                                                        .pickImage(
                                                      source:
                                                          ImageSource.gallery,
                                                    );
                                                    Navigator.pop(context);
                                                  },
                                                  icon: Icon(Icons.image),
                                                ),
                                                Text("Galery")
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      actions: <Widget>[
                                        FlatButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Text(
                                            "Cancel",
                                            style:
                                                TextStyle(color: Colors.green),
                                          ),
                                        ),
                                      ],
                                    );
                                  });
                              if (image != null) {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    });
                                print("upload iamge avatar");
                                var resStream =
                                    await _bloc.updateLoadImage(image);
                                resStream.stream
                                  ..transform(utf8.decoder).listen((value) {
                                    print(value.toString());
                                  }).onError((e) {
                                    print(e.toString());
                                  });
                                await _bloc.loadUserInfo();
                                Navigator.pop(context);
                                print("done upload");
                              } else {
                                print("image = null");
                              }
                            },
                            child: Icon(
                              Icons.camera_alt,
                              color: Colors.greenAccent,
                            )),
                      )
                    ],
                  ),
                )),
            Expanded(flex: 2, child: Container())
          ],
        ),
      );

  Widget bottomView() => Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            //itemTextRow(head, tail, isTop, isBottom)
            itemTextRow(
                textHead("Họ và tên :"),
                textTail("${widget.user?.ho ?? ""} ${widget.user?.ten ?? ""}"),
                true,
                false),
            Divider(
              height: 1,
            ),
            itemTextRow(textHead("Email :"),
                textTail("${widget.user?.email ?? ""}"), true, false),
            Divider(
              height: 1,
            ),
            itemTextRow(textHead("Số điện thoại :"),
                textTail("${widget.user?.dienThoai ?? ""}"), true, false),
            Divider(
              height: 1,
            ),
          ],
        ),
      );

  Widget contentMain() => Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: topView(),
          ),
          Expanded(
            flex: 1,
            child: bottomView(),
          )
        ],
      );

  Widget textHead(String text) {
    return Text(
      text,
      style: TextStyle(fontSize: 18.0),
    );
  }

  Widget textTail(String text) {
    return Text(text,
        textAlign: TextAlign.end,
        style: TextStyle(
          fontSize: 18.0,
        ));
  }

  Widget itemTextRow(
    Widget head,
    Widget tail,
    bool isTop,
    bool isBottom,
  ) {
    return Container(
      height: 56.0,
      padding:
          EdgeInsets.only(top: isTop ? 16.0 : 8.0, bottom: isTop ? 16.0 : 8.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: head,
          ),
          Expanded(
            flex: 1,
            child: tail,
          )
        ],
      ),
    );
  }

  ProfileEditBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = ProfileEditBloc();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Tài khoản"),
        /* actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.edit,
              color: Colors.white,
            ),
          )
        ],*/
      ),
      body: contentMain(),
    );
  }
}
