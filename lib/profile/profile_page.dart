import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/profile/feedback/feedBack.dart';
import 'package:project_aparment/profile/notebook/notebook_page.dart';
import 'package:project_aparment/profile/profile_bloc.dart';
import 'package:project_aparment/profile/vehicle/vehicle_page.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _ProfileFul();
  }
}

class _ProfileFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileState();
}

class _ProfileState extends State<_ProfileFul> {
  ProfileBloc _bloc;

  @override
  void initState() {
    super.initState();
  }

  Widget topView() => Container(
        padding: const EdgeInsets.only(
            top: 35.0, bottom: 16.0, left: 16.0, right: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
                Expanded(
                  flex: 1,
                  child: AspectRatio(
                    aspectRatio: 1.0,
                    child: StreamBuilder<User>(
                      stream: _bloc.userStream,
                      builder: (context, snapshot) {
                        return CircleAvatar(
                          backgroundColor: Colors.grey,
                          backgroundImage: CachedNetworkImageProvider(
                            (snapshot.data != null)
                                ? snapshot.data.photoUrl
                                : "",
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Expanded(flex: 1, child: Container())
              ],
            ),
            Container(
              padding: const EdgeInsets.only(top: 8.0),
              child: StreamBuilder<User>(
                stream: _bloc.userStream,
                builder: (context, snapshot) {
                  var text = snapshot != null && snapshot.data != null
                      ? snapshot.data.ten
                      : "";
                  return Text(
                    text,
                    style: TextStyle(
                        color: const Color(0xde000000),
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        fontSize: 24.0),
                  );
                },
              ),
            ),
          ],
        ),
      );

  Widget bottomView() => Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Divider(
                  height: 1.0,
                ),
                InkWell(
                    onTap: () async {
                      await Navigator.pushNamed(context, NAVIGATOR_EDIT_PROFILE,
                          arguments: _bloc.user);
                      _bloc.loadUserInfo();
                    },
                    child: itemTextRow(itemText("Thông tin cá nhân"),
                        Icon(Icons.chevron_right), true, false)),
                Divider(
                  height: 1.0,
                ),
                InkWell(
                  onTap: () => Navigator.pushNamed(context, '/apartment'),
                  child: itemTextRow(itemText("Căn hộ"),
                      Icon(Icons.chevron_right), false, false),
                ),
                Divider(
                  height: 1.0,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NoteBookPage()));
                  },
                  child: itemTextRow(itemText("Sổ tay cư dân"),
                      Icon(Icons.chevron_right), false, false),
                ),
                Divider(
                  height: 1.0,
                ),
                InkWell(
                  onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FeedBack())),
                  child: itemTextRow(itemText("Góp ý"),
                      Icon(Icons.chevron_right), false, false),
                ),
                Divider(
                  height: 1.0,
                ),
                Divider(
                  height: 1.0,
                ),
                InkWell(
                  onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => VehiclePage())),
                  child: itemTextRow(
                      itemText("Xe"), Icon(Icons.chevron_right), false, false),
                ),
                Divider(
                  height: 1.0,
                ),
                InkWell(
                  onTap: () {},
                  child: itemTextRow(
                      itemText("Nhận thông báo phí :"),
                      StreamBuilder<bool>(
                        stream: _bloc.billSubscribeStream,
                        initialData: true,
                        builder: (context, snapshot) {
                          return Switch(
                            activeColor: Color(0xffF7941D),
                            value: snapshot.data,
                            onChanged: (isCheck) =>
                                _bloc.billSubscribeIn.add(isCheck),
                          );
                        },
                      ),
                      false,
                      false),
                ),
                Divider(
                  height: 1.0,
                ),
                InkWell(
                  onTap: () {},
                  child: itemTextRow(
                      itemText("Nhận thông báo khác :"),
                      StreamBuilder<bool>(
                        stream: _bloc.newsSubscribeStream,
                        initialData: false,
                        builder: (context, snapshot) {
                          return Switch(
                            activeColor: Color(0xffF7941D),
                            value: snapshot.data,
                            onChanged: (isCheck) =>
                                _bloc.newsSubscribeIn.add(isCheck),
                          );
                        },
                      ),
                      false,
                      true),
                ),
                Divider(
                  height: 1,
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () => _showPopupLogout(),
            child: itemTextRow(
                Text(
                  "Đăng xuất",
                  style:
                      const TextStyle(color: Colors.redAccent, fontSize: 18.0),
                ),
                null,
                false,
                false),
          )
        ],
      );

  _showPopupLogout() => showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text(
            "Bạn có chắc muốn đăng xuất",
            style: Theme.of(context).textTheme.title,
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("Không"),
            ),
            FlatButton(
              child: Text("Có"),
              onPressed: () {
                _bloc.logOut();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    NAVIGATOR_LOGIN, (route) => route.isFirst);
              },
            )
          ],
        );
      });

  Widget itemText(String text) => Text(
        text,
        style: TextStyle(color: Colors.black38),
      );

  Widget itemTextRow(
    Widget head,
    Widget tail,
    bool isTop,
    bool isBottom,
  ) {
    return Container(
      height: 56.0,
      padding: EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          top: isTop ? 16.0 : 8.0,
          bottom: isTop ? 16.0 : 8.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: head,
          ),
          tail != null ? tail : Container()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _bloc = BlocProvider.of<ProfileBloc>(context);
    return Scaffold(
      body: BlocProvider(
        bloc: _bloc,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: topView(),
            ),
            Expanded(
              flex: 7,
              child: bottomView(),
            )
          ],
        ),
      ),
    );
  }
}
