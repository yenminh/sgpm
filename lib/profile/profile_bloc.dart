import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileBloc extends BlocBase {
  Api _api = Api.instance;
  User user;
  BehaviorSubject<bool> _billSubscribeBS = BehaviorSubject();

  Sink<bool> get billSubscribeIn => _billSubscribeBS.sink;

  Stream<bool> get billSubscribeStream => _billSubscribeBS.stream;

  //subscribe notification
  BehaviorSubject<bool> _newsSubscribeBS = BehaviorSubject();

  Sink<bool> get newsSubscribeIn => _newsSubscribeBS.sink;

  Stream<bool> get newsSubscribeStream => _newsSubscribeBS.stream;

  //subscribe user
  BehaviorSubject<User> _userBehavior = BehaviorSubject();

  Sink<User> get _userIn => _userBehavior.sink;

  Stream<User> get userStream => _userBehavior.stream;

  ProfileBloc() {
    loadUserInfo();
  }

  Future<void> loadUserInfo() async {
    _api.loadUser().then((result) {
      if (result != null) {
        user = result.data;
        _userIn.add(user);
      }
    }).catchError((ex) {
      print(ex.toString());
    });
  }

  logOut() {
    SharedPreferences.getInstance().then((pref) {
      pref.clear();
    });
  }

  @override
  void dispose() {
    _userBehavior.close();
    _billSubscribeBS.close();
    _newsSubscribeBS.close();
  }
}
