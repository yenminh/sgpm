import 'package:project_aparment/data/canho.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class ApartmentBloc extends BlocBase {
  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<Apartment> _apartmentBehaviorSubject = BehaviorSubject();

  Sink<Apartment> get _apartmentServiceIn => _apartmentBehaviorSubject.sink;

  Stream<Apartment> get apartmentStream => _apartmentBehaviorSubject.stream;

  var _api = Api.instance;

  ApartmentBloc() {
    loadApartment();
  }

  Future<void> loadApartment() async {
    _stateIn.add(STATE_LOADING);
    _api.loadCanHo().then((result) {
      if (result != null) {
        _apartmentServiceIn.add(Apartment.fromJson(Map<String, dynamic>.from(result.data)));
      }
      _stateIn.add(STATE_SUCCESS);
    }).catchError((ex) {
      _stateIn.add(STATE_ERROR);
      print(ex.toString());
    });
  }

  @override
  void dispose() {
    _stateBehaviorSubject.close();
    _apartmentBehaviorSubject.close();
  }
}
