import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:project_aparment/data/canho.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/profile/apartment/apartment_bloc.dart';

class ApartmentWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _ApartmentFul();
  }
}

class _ApartmentFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ApartmentState();
  }
}

class _ApartmentState extends State<_ApartmentFul> {
  Apartment apartment;

  Widget socialWidget() {
    var textStyleTop = const TextStyle(fontSize: 22);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 16.0, top: 16.0, right: 8.0),
          child: Icon(
            Icons.favorite,
            color: Colors.redAccent,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
              top: 16.0, bottom: 16.0, right: 8.0, left: 8.0),
          child: Text(
            "232",
            style: textStyleTop,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
              top: 16.0, bottom: 16.0, right: 8.0, left: 8.0),
          child: Icon(
            Icons.comment,
            color: Colors.black38,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
              top: 16.0, bottom: 16.0, right: 16.0, left: 8.0),
          child: Text(
            "123",
            style: textStyleTop,
          ),
        )
      ],
    );
  }

  Widget chips() => Row(
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(left: 16.0, right: 8.0, bottom: 16.0),
            child: Chip(
              backgroundColor: Colors.red[300],
              label: Text("Mã căn hộ:  ${apartment?.ma ?? ""}" ?? "",
                  style: TextStyle(color: Colors.white)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 16.0),
            child: Chip(
              backgroundColor: Colors.red[300],
              label: Text(
                "Loại căn hộ : ${(apartment != null) ? (apartment.loai == 2) ? "Chủ sở hữu" : "Cho thuê" : ""}",
                style: TextStyle(color: Colors.white),
              ),
            ),
          )
        ],
      );

  ApartmentBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = ApartmentBloc();
    _bloc.apartmentStream.listen((data) {
      if (data != null) {
        setState(() {
          apartment = data;
        });
      }
    });
  }

  Widget contentWidget() => Column(
        children: <Widget>[
          //socialWidget(),
          chips(),
          Expanded(
            child: Container(
              width: double.infinity,
              alignment: Alignment(-1.0, -1.0),
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              child: Text(
                apartment?.mota ?? "",
                style: TextStyle(color: Colors.black87, fontSize: 16.0),
              ),
            ),
          ),
          bottomWidget()
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(200.0),
        child: AppBar(
          flexibleSpace: Image.asset(
            'images/image_apartment.jpg',
            fit: BoxFit.cover,
            height: 200 + AppBar().preferredSize.height,
          ),
        ),
      ),
      body: StreamBuilder<int>(
        initialData: STATE_INIT,
        stream: _bloc.stateStream,
        builder: (context, snapshot) {
          if (snapshot.data == STATE_LOADING)
            return Center(child: CircularProgressIndicator());
          return contentWidget();
        },
      ),
    );
  }

  List<Widget> listItems() {
    if (apartment == null) return List.generate(1, (x) => Container());

    return List.generate(apartment.cudan.length, (index) {
      print("${apartment.cudan[index].photo}");
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.all(16.0),
            width: 56,
            height: 56,
            child: CircleAvatar(
                backgroundImage: CachedNetworkImageProvider(
              apartment.cudan[index].photoUrl,
            )),
          ),
          Text(
            apartment.cudan[index].mota,
            style: TextStyle(fontSize: 16.0),
          )
        ],
      );
    });
  }

  Widget bottomWidget() {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
            child: Text(
              "Nhân khẩu",
              style:
                  const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            height: 120.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: listItems(),
            ),
          )
        ],
      ),
    );
  }
}
