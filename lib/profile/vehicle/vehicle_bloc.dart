import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/data/vehicle_model.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class VehicleBloc extends BlocBase {
  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<List<VehicleModel>> _behaviorSubject = BehaviorSubject();
  Stream<List<VehicleModel>> get vehicleStream => _behaviorSubject.stream;
  List<VehicleModel> _listData = List();
  int currentPage = 1;
  String nextPage;
  VehicleBloc(){
    loadVehicle();
  }

  Future<void> loadVehicle() async {
    _stateIn.add(STATE_LOADING);
    Api.instance.loadListVehicle(1).then((result) {
      if (result.status == 200) {
        currentPage = result.data.currentPage;
        nextPage = result.data.nextPageUrl;
        _listData = result.data.data;
        _behaviorSubject.add(_listData);
        _stateIn.add(STATE_SUCCESS);
      } else {
        _stateIn.add(STATE_EMPTY);
      }
    }).catchError((error) {
      print(error.toString());
    });
  }

  Future<void> loadMoreVehicle() async {
    if (nextPage != null && nextPage.isNotEmpty) {
      currentPage++;
      Api.instance.loadListVehicle(currentPage).then((result) {
        if (result.status == 200) {
          currentPage = result.data.currentPage;
          nextPage = result.data.nextPageUrl;
          _listData.addAll(result.data.data);
          _behaviorSubject.add(_listData);
          _stateIn.add(STATE_SUCCESS);
        } else {
          _stateIn.add(STATE_EMPTY);
        }
      }).catchError((error) {
        print(error.toString());
      });
    }
  }

  @override
  void dispose() {
    _behaviorSubject.close();
    _stateBehaviorSubject.close();
  }
}
