import 'package:flutter/material.dart';
import 'package:project_aparment/data/vehicle_model.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/profile/vehicle/vehicle_bloc.dart';

class VehiclePage extends StatefulWidget {
  @override
  _VehiclePageState createState() => _VehiclePageState();
}

class _VehiclePageState extends State<VehiclePage> {
  VehicleBloc _bloc;

  Widget item(VehicleModel item) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        key: Key(item.id?.toString() ?? ""),
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Biển số :",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
              Text(
                "${item.bienso ?? ""}",
                style: TextStyle(fontSize: 14.0),
              ),
            ],
          ),
          Container(
            height: 4.0,
          ),
          Row(
            children: <Widget>[
              Text(
                "Nơi đăng ký:",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
              Text(
                " ${item.noidangky??""}",
                style: TextStyle(fontSize: 14.0),
              ),
            ],
          ),
          Container(
            height: 4.0,
          ),
          Row(
            children: <Widget>[
              Text(
                "Số khung: ",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
              Text(
                "${item.sokhung??""}",
                style: TextStyle(fontSize: 14.0),
              ),
              Text(
                "  Số máy:",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
              Text(
                " ${item.somay??""}",
                style: TextStyle(fontSize: 14.0),
              ),
            ],
          ),
          Container(
            height: 4.0,
          ),
          Row(
            children: <Widget>[
              Text(
                "Giá tiền:",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
              Text(
                " ${item.giatien??""}",
                style: TextStyle(fontSize: 14.0),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _bloc = VehicleBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Xe"),
      ),
      body: StreamBuilder<int>(
        initialData: STATE_INIT,
        stream: _bloc.stateStream,
        builder: (context, snapshot) {
          if (snapshot.data == STATE_LOADING)
            return Center(
              child: CircularProgressIndicator(),
            );
          return StreamBuilder<List<VehicleModel>>(
            initialData: List(),
            stream: _bloc.vehicleStream,
            builder: (context, snapshot) {
              return ListView.separated(
                itemBuilder: (context, index) {
                  if (index == snapshot.data.length - 2 &&
                      _bloc.nextPage != null &&
                      _bloc.nextPage.isNotEmpty) {
                    _bloc.loadMoreVehicle();
                  }
                  return item(snapshot.data[index]);
                },
                separatorBuilder: (context, index) => Divider(
                  indent: 16,
                  endIndent: 16,
                ),
                itemCount: snapshot.data.length,
              );
            },
          );
        },
      ),
    );
  }
}
