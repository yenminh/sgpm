import 'package:flutter/material.dart';
import 'package:project_aparment/data/repository/api.dart';

class FeedBack extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FeedBackState();
}

class _FeedBackState extends State<FeedBack> {
  TextEditingController _titleController;
  TextEditingController _bodyController;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _bodyController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text("Mục góp ý"),
        ),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                child: Text(
                  "Tiêu đề:",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(16.0),
                child: TextField(
                    controller: _titleController,
                    maxLines: 2,
                    decoration: InputDecoration(
                      counterText: "",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          borderSide: BorderSide(
                            width: 1,
                          )),
                    )),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                child: Text(
                  "Nội dung:",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(16.0),
                child: TextField(
                    controller: _bodyController,
                    maxLines: 4,
                    maxLength: 300,
                    decoration: InputDecoration(
                      counterText: "",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          borderSide: BorderSide(
                            width: 1,
                          )),
                    )),
              ),
              Container(
                padding: const EdgeInsets.all(16.0),
                width: double.infinity,
                child: RaisedButton(
                    padding: const EdgeInsets.all(16.0),
                    color: Colors.green,
                    onPressed: () async {
                      showDialog(
                          context: (context),
                          builder: (context) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          });
                      //
                      var title = _titleController.text;
                      var body = _bodyController.text;
                      if (title != null &&
                          body != null &&
                          title.isNotEmpty &&
                          body.isNotEmpty) {
                        var res = await Api.instance.postFeedBack(title, body);
                        Navigator.pop(context);
                        if (res != null && res) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  content: Text(
                                      "Cám ơn bạn đã góp ý cho chúng tôi. BQT sẽ hồi âm lại với bạn trong thời gian sớm nhất"),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(
                                        "Ok",
                                        style:
                                            TextStyle(color: Colors.redAccent),
                                      ),
                                      onPressed: () => Navigator.popUntil(
                                          context, (route) => route.isFirst),
                                    )
                                  ],
                                );
                              });
                        } else {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  content: Text(
                                      "Đã có lỗi xảy ra vui long thử lại sau"),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(
                                        "Ok",
                                        style:
                                            TextStyle(color: Colors.redAccent),
                                      ),
                                      onPressed: () => Navigator.popUntil(
                                          context, (route) => route.isFirst),
                                    )
                                  ],
                                );
                              });
                        }
                      }

                      //
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Gửi liên hệ".toUpperCase(),
                      style: TextStyle(color: Colors.white),
                    )),
              )
            ],
          ),
        ));
  }
}
