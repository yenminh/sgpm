import 'package:flutter/material.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/home/home.dart';
import 'package:project_aparment/login/login_widget.dart';
import 'package:project_aparment/notification/news/news_detail.dart';
import 'package:project_aparment/notification/notification/notifycation_detail.dart';
import 'package:project_aparment/pay/payment_page.dart';
import 'package:project_aparment/profile/apartment/apartment_page.dart';
import 'package:project_aparment/profile/edit/profile_edit_page.dart';
import 'package:project_aparment/service/service_detail.dart';
import 'package:shared_preferences/shared_preferences.dart';

const int STATE_INIT = 0;
const int STATE_LOADING = 2;
const int STATE_SUCCESS = 3;
const int STATE_EMPTY = 4;
const int STATE_ERROR = 5;
//navigator
const String NAVIGATOR_HOME = "/home";
const String NAVIGATOR_LOGIN = "/login";
const String NAVIGATOR_APARTMENT = "/apartment";
const String NAVIGATOR_EDIT_PROFILE = "/editProfile";
const String NAVIGATOR_PAYMENT = "/paymentMethod";
const String NAVIGATOR_NEWS_DETAIL = "/newsDetail";
const String NAVIGATOR_NOTIFICATION_DETAIL = "/notificationDetail";
const String NAVIGATOR_SERVICES_DETAIL = "/serviceDetail";
//

MaterialPageRoute buildRoute(RouteSettings settings, Widget builder) {
  return new MaterialPageRoute(
    settings: settings,
    builder: (ctx) => builder,
  );
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.getInstance().then((prefs) {
    var token = prefs.getString(KEY_TOKEN);
    var root = token != null && token.isNotEmpty ? Home() : LoginPage();
    runApp(MaterialApp(
      title: 'Apparment',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        if (settings.name == NAVIGATOR_NEWS_DETAIL) {
          return buildRoute(
              settings,
              NewsDetails(
                news: settings.arguments,
              ));
        } else if (settings.name == NAVIGATOR_EDIT_PROFILE) {
          return buildRoute(
              settings,
              ProfileEditPage(
                user: settings.arguments,
              ));
        } else if (settings.name == NAVIGATOR_NOTIFICATION_DETAIL) {
          return buildRoute(
              settings,
              NotificationDetail(
                settings.arguments,
              ));
        } else if (settings.name == NAVIGATOR_SERVICES_DETAIL) {
          return buildRoute(settings, ServiceDetailPage(settings.arguments));
        }
        return null;
      },
      initialRoute: '/',
      routes: {
        '/': (context) => root,
        NAVIGATOR_LOGIN: (context) => LoginPage(),
        NAVIGATOR_HOME: (context) => Home(),
        NAVIGATOR_APARTMENT: (context) => ApartmentWidget(),
        //NAVIGATOR_EDIT_PROFILE: (context) => ProfileEditPage(),
        NAVIGATOR_PAYMENT: (context) => PaymentPage()
        //NAVIGATOR_NEWS_DETAIL: (context) => NewsDetails()
      },
      theme: ThemeData(
        primaryColor: Colors.red,
        primarySwatch: Colors.deepOrange,
      ),
    ));
  });
}

abstract class BlocBase {
  ///clear and close all resource please. Make sure we closed all stream after this method
  void dispose();
}

class BlocProvider<T extends BlocBase> extends InheritedWidget {
  final T bloc;

  BlocProvider({Key key, @required T bloc, Widget child})
      : this.bloc = bloc,
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static Type _typeOf<T>() => T;

  static T of<T extends BlocBase>(BuildContext context) {
    var type = _typeOf<BlocProvider<T>>();
    return (context.inheritFromWidgetOfExactType(type) as BlocProvider<T>).bloc;
  }
}
