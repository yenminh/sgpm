import 'package:flutter/material.dart';
import 'package:project_aparment/data/service.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/profile/profile_bloc.dart';
import 'package:project_aparment/service/service_bloc.dart';
import 'package:project_aparment/service/service_detail.dart';

class ServicePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _ServiceFul();
  }
}

class _ServiceFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ServiceState();
}

class _ServiceState extends State<_ServiceFul> {
  ServiceBloc _bloc;
  ProfileBloc _proFileBloc;

  @override
  void initState() {
    super.initState();
    _bloc = ServiceBloc();
  }

  Widget textItem(Service data) {
    var textStyle = const TextStyle(
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
    );
    return Text(
      data.ten ?? "",
      style: textStyle,
      textAlign: TextAlign.center,
    );
  }

  Widget imageItem(String url) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Image.network(
        url,
        fit: BoxFit.contain,
      ),
    );
  }

  List<Widget> litItem(List<Service> listService) => List.generate(
      listService.length,
      (index) => Card(
          elevation: 2.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(3.0))),
          child: InkWell(
            onTap: () => Navigator.pushNamed(context, NAVIGATOR_SERVICES_DETAIL,
                arguments: ServiceScreenArguments(listService[index], _proFileBloc.user)),
            child: Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1.06,
                  child: imageItem(listService[index].photoUrl),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment(0.0, 1.0),
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: textItem(listService[index]),
                    ),
                  ),
                )
              ],
            ),
          )));

  Widget body(List<Service> listService) => GridView.count(
        mainAxisSpacing: 8.0,
        crossAxisSpacing: 8.0,
        childAspectRatio: 0.78,
        crossAxisCount: 2,
        children: litItem(listService),
      );

  @override
  Widget build(BuildContext context) {
    _proFileBloc = BlocProvider.of<ProfileBloc>(context);
    return Column(
      children: <Widget>[
        AppBar(
          title: Text("Dịch vụ"),
        ),
        Expanded(
          child: Container(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: StreamBuilder<int>(
              initialData: STATE_INIT,
              stream: _bloc.stateStream,
              builder: (context, snapshot) {
                if (snapshot.data == STATE_LOADING)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                return StreamBuilder<List<Service>>(
                  initialData: List(),
                  stream: _bloc.listServiceStream,
                  builder: (context, snapshot) {
                    return body(snapshot.data);
                  },
                );
              },
            ),
          ),
        )
      ],
    );
  }
}
