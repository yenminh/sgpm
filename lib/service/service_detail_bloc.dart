import 'dart:io';

import 'package:http/http.dart';
import 'package:project_aparment/data/book_service_reponse.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/data/service_detail.dart';
import 'package:project_aparment/data/service_provider.dart';
import 'package:rxdart/rxdart.dart';

import '../main.dart';

class ServiceDetailBloc extends BlocBase {

  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<int> _stateProviderBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateProviderIn => _stateProviderBehaviorSubject.sink;

  Stream<int> get stateProviderStream => _stateProviderBehaviorSubject.stream;

  //
  BehaviorSubject<List<ServiceDetail>> _listServiceBehaviorSubject = BehaviorSubject();

  Sink<List<ServiceDetail>> get _listServiceIn => _listServiceBehaviorSubject.sink;

  Stream<List<ServiceDetail>> get listServiceStream => _listServiceBehaviorSubject.stream;

  //
  BehaviorSubject<List<ServiceProvider>> _listServiceProviderBehaviorSubject = BehaviorSubject();

  Sink<List<ServiceProvider>> get _listServiceProviderIn => _listServiceProviderBehaviorSubject.sink;

  Stream<List<ServiceProvider>> get listServiceProviderStream => _listServiceProviderBehaviorSubject.stream;

  var _api = Api.instance;
  int id;

  ServiceDetailBloc(this.id) {
    loadService();
  }

  Future<void> loadService() async {
    _stateIn.add(STATE_LOADING);
    var res = await _api.loadListServiceDetail(id.toString());
    if (res != null) {
      _listServiceIn.add(res.data.map((item) => ServiceDetail.fromJson(Map<String, dynamic>.from(item))).toList());
      _stateIn.add(STATE_SUCCESS);
    } else {
      _stateIn.add(STATE_EMPTY);
    }
  }

  Future<void> loadServiceProvider(int id) async {
    _stateProviderIn.add(STATE_LOADING);
    var res = await _api.loadListServiceProvider(id);
    if (res != null) {
      _listServiceProviderIn.add(res.data.map((item) => ServiceProvider.fromJson(Map<String, dynamic>.from(item))).toList());
      _stateProviderIn.add(STATE_SUCCESS);
    } else {
      _stateProviderIn.add(STATE_EMPTY);
    }
  }

  Future<StreamedResponse> bookService({int idService, String number, DateTime time, String description, File image, int provider}) async {
    return _api.bookSv(
        idService: idService, number: number, time: time, description: description, image: image, provider: provider);
  }

  @override
  void dispose() {
    _listServiceProviderBehaviorSubject.close();
    _stateProviderBehaviorSubject.close();
    _listServiceBehaviorSubject.close();
    _stateBehaviorSubject.close();
  }
}
