import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/data/service.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class ServiceBloc extends BlocBase {

  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<List<Service>> _listServiceBehaviorSubject = BehaviorSubject();

  Sink<List<Service>> get _listServiceIn => _listServiceBehaviorSubject.sink;

  Stream<List<Service>> get listServiceStream => _listServiceBehaviorSubject.stream;

  var _api = Api.instance;

  ServiceBloc() {
    loadService();
  }

  Future<void> loadService() async {
    _stateIn.add(STATE_LOADING);
    var res = await _api.loadListService();
    if (res != null) {
      _listServiceIn.add(res.data.map((item) => Service.fromJson(Map<String, dynamic>.from(item))).toList());
      _stateIn.add(STATE_SUCCESS);
    } else {
      _stateIn.add(STATE_EMPTY);
    }
  }



  @override
  void dispose() {
    _listServiceBehaviorSubject.close();
    _stateBehaviorSubject.close();
  }
}
