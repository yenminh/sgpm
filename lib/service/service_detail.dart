import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project_aparment/data/book_service_reponse.dart';
import 'package:project_aparment/data/service.dart';
import 'package:project_aparment/data/service_detail.dart';
import 'package:project_aparment/data/service_provider.dart';
import 'package:project_aparment/data/user.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/service/service_detail_bloc.dart';

class ServiceScreenArguments {
  Service _service;
  User _user;

  ServiceScreenArguments(this._service, this._user);
}

class ServiceDetailPage extends StatefulWidget {
  Service _service;
  User _user;

  ServiceDetailPage(ServiceScreenArguments agrs) {
    this._service = agrs._service;
    this._user = agrs._user;
  }

  @override
  State<StatefulWidget> createState() => _ServiceDetailPageState();
}

class _ServiceDetailPageState extends State<ServiceDetailPage> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  ServiceDetailBloc _bloc;
  ServiceDetail _selected;
  ServiceProvider _providerSelected;
  String description;
  int number;
  int time;
  DateTime _dateTime;
  File _image;
  var timeFormatter = DateFormat('dd-MM-yyyy – hh:mm');
  PageController _controller;
  TextEditingController _textEditNumberController;
  TextEditingController _textEditDescriptionController;

  @override
  void initState() {
    super.initState();
    _bloc = ServiceDetailBloc(widget._service.id);
    _controller = PageController();
    _textEditNumberController = TextEditingController();
    _textEditDescriptionController = TextEditingController();
  }

  Widget textItem(ServiceDetail data) {
    var textStyle = const TextStyle(
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
    );
    return Text(
      data.ten ?? "",
      style: textStyle,
      textAlign: TextAlign.center,
    );
  }

  Widget imageItem(String url) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Image.network(
        url,
        fit: BoxFit.contain,
      ),
    );
  }

  Widget profileInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          widget._service.ten,
          style: Theme.of(context).textTheme.title,
        ),
        Container(
          height: 16.0,
        ),
        Text("Thông tin khách hàng:"),
        Container(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Họ tên: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
                "${widget._user?.ten ?? ""}  ${(widget._user?.mota != null && widget._user.mota.isNotEmpty) ? "- ${widget._user.mota}" : ""}"),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Chung cư: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text('''${widget._user?.building?.ten ?? ""}'''),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Block: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text('''${widget._user?.block?.ten ?? ""}'''),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Mã căn hộ: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text('''${widget._user?.apartment?.code ?? ""}'''),
          ],
        ),
      ],
    );
  }

  Widget descriptionItem() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
            padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
            child: Text("Mô tả:")),
        Container(
          child: TextField(
            controller: _textEditDescriptionController,
            maxLines: 4,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(3.0),
                  borderSide: BorderSide(
                    width: 1,
                  )),
            ),
          ),
        ),
      ],
    );
  }

  Widget providers() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text("Lựa chọn nhà cung cấp: "),
        Container(
          height: 8,
        ),
        StreamBuilder<int>(
          initialData: STATE_INIT,
          stream: _bloc.stateProviderStream,
          builder: (context, snapshot) {
            if (snapshot.data == STATE_LOADING)
              return Center(
                child: CircularProgressIndicator(),
              );
            return StreamBuilder<List<ServiceProvider>>(
              initialData: List(),
              stream: _bloc.listServiceProviderStream,
              builder: (context, snapshot) {
                return bodyProvider(snapshot.data);
              },
            );
          },
        )
      ],
    );
  }

  Widget bottomForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text("Số lượng: "),
        Container(
          height: 26,
          width: 50,
          padding: const EdgeInsets.only(top: 4.0),
          child: TextField(
              maxLength: 3,
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              controller: _textEditNumberController,
              decoration: InputDecoration(
                counterText: "",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(3.0),
                    borderSide: BorderSide(
                      width: 1,
                    )),
              )),
        ),
        Row(
          children: <Widget>[
            Text("Thời gian: "),
            IconButton(
              icon: Icon(Icons.calendar_today),
              onPressed: () async {
                Future<DateTime> selectedDate = showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2019),
                  lastDate: DateTime(2021),
                  builder: (BuildContext context, Widget child) {
                    return Theme(
                      data: ThemeData.dark(),
                      child: child,
                    );
                  },
                );
                var date = await selectedDate;
                var time = await showTimePicker(
                    context: context, initialTime: TimeOfDay.now());
                setState(() {
                  print(DateTime.now().toIso8601String());
                  DateTime.utc(
                      date.year, date.month, date.day, time.hour, time.minute);
                  _dateTime = DateTime.utc(
                      date.year, date.month, date.day, time.hour, time.minute);
                });
              },
            ),
          ],
        ),
        Text("${_dateTime != null ? timeFormatter.format(_dateTime) : ""}"),
        Row(
          children: <Widget>[
            Container(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                child: Text("Hinh ảnh:")),
            IconButton(
              icon: Icon(
                Icons.camera_alt,
                color: Theme.of(context).accentColor,
              ),
              onPressed: () async {
                var image =
                    await ImagePicker.pickImage(source: ImageSource.camera);
                setState(() {
                  _image = image;
                });
              },
            )
          ],
        ),
        Container(
          padding: const EdgeInsets.only(right: 16.0),
          child: _image != null
              ? Container(
                  width: 150,
                  height: 150,
                  child: Image.file(_image),
                )
              : Container(
                  width: 150,
                  height: 150,
                  color: Colors.grey,
                ),
        )
      ],
    );
  }

  Widget form() {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: ListView(
            children: <Widget>[
              profileInfo(),
              descriptionItem(),
              Container(
                height: 16.0,
              ),
              providers(),
              Container(
                height: 16.0,
              ),
              bottomForm()
            ],
          )),
    );
  }

  List<Widget> litItem(List<ServiceDetail> listService) => List.generate(
      listService.length,
      (index) => Card(
          color:
              listService[index] == _selected ? Colors.blue[100] : Colors.white,
          elevation: 2.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(3.0))),
          child: InkWell(
            onTap: () {
              setState(() {
                _selected = listService[index];
                _bloc.loadServiceProvider(_selected.id);
                _controller.animateToPage(1,
                    duration: Duration(milliseconds: 300), curve: Curves.ease);
              });
            },
            child: Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1.06,
                  child: imageItem(listService[index].photoUrl),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment(0.0, 1.0),
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: textItem(listService[index]),
                    ),
                  ),
                )
              ],
            ),
          )));

  Widget body(List<ServiceDetail> listService) => GridView.count(
        mainAxisSpacing: 8.0,
        crossAxisSpacing: 8.0,
        childAspectRatio: 0.78,
        crossAxisCount: 2,
        children: litItem(listService),
      );

  Widget textItemProvider(ServiceProvider data) {
    var textStyle = const TextStyle(
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
    );
    return Text(
      data.ten ?? "",
      style: textStyle,
      textAlign: TextAlign.start,
    );
  }

  Widget imageItemProvider(String url) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Image.network(
        url,
        fit: BoxFit.contain,
      ),
    );
  }

  List<Widget> litItemProvider(List<ServiceProvider> listService) =>
      List.generate(
        listService.length,
        (index) => Card(
          color: listService[index] == _providerSelected
              ? Colors.blue[100]
              : Colors.white,
          elevation: 2.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(3.0))),
          child: InkWell(
            onTap: () {
              setState(() {
                _providerSelected = listService[index];
              });
            },
            child: ListTile(
              leading: SizedBox(
                width: 56,
                height: 56,
                child: imageItem(listService[index].imageUrl),
              ),
              title: textItemProvider(listService[index]),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                  "${listService[index].gia}",
                  style: TextStyle(fontSize: 14),
                ),
              ),
            ),
          ),
        ),
      );

  Widget bodyProvider(List<ServiceProvider> listService) => ListView(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: litItemProvider(listService),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget._service.ten),
      ),
      body: StreamBuilder<int>(
        initialData: STATE_INIT,
        stream: _bloc.stateStream,
        builder: (context, snapshot) {
          if (snapshot.data == STATE_LOADING)
            return Center(
              child: CircularProgressIndicator(),
            );
          return Column(
            children: <Widget>[
              Expanded(
                  child: PageView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: _controller,
                      children: <Widget>[
                    StreamBuilder<List<ServiceDetail>>(
                      initialData: List<ServiceDetail>(),
                      stream: _bloc.listServiceStream,
                      builder: (context, snapshot) {
                        return GridView.count(
                          mainAxisSpacing: 8.0,
                          crossAxisSpacing: 8.0,
                          childAspectRatio: 0.78,
                          crossAxisCount: 2,
                          children: litItem(snapshot.data),
                        );
                      },
                    ),
                    form()
                  ])),
              bottomButton()
            ],
          );
        },
      ),
    );
  }

  Widget bottomButton() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16.0),
      child: RaisedButton(
        color: Theme.of(context).primaryColor,
        padding: const EdgeInsets.all(12.0),
        child: Text(
          "ĐẶT DỊCH VỤ",
          style: const TextStyle(color: Colors.white),
        ),
        onPressed: () async {
          showDialog(
              context: context,
              builder: (context) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              });
          if (_textEditNumberController.text != null &&
              _selected != null &&
              _providerSelected != null &&
              //_image != null &&
              _dateTime != null &&
              _textEditDescriptionController.text != null) {
            var resStream = await _bloc.bookService(
                number: _textEditNumberController.text,
                idService: _selected.id,
                provider: _providerSelected.id,
                image: _image,
                time: _dateTime,
                description: _textEditDescriptionController.text);
            resStream.stream
              ..transform(utf8.decoder).listen((value) {
                Map json = jsonDecode(value);
                print("${json["data"]}");
                var res = BookServiceReponse.fromJson(json["data"]);
                print("res $res");
                if (res != null) {
                  Navigator.pop(context);
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text("Đặt dịch vụ thành công"),
                          content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text("Tên dịch vụ : ${widget._service.ten}"),
                              Text("Nhà cung cấp : ${_providerSelected.ten}"),
                              Container(height: 4),
                              Text(
                                "Họ tên: ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                  "${widget._user.ten}  - ${widget._user.mota}"),
                              Container(
                                height: 4,
                              ),
                              Text("Mô tả : ${res.mota ?? ""}"),
                              Container(
                                height: 4,
                              ),
                              Text("Giá : ${res.dongia}"),
                              Flexible(
                                child: Text(
                                  "Chi phí tạm tính đã được ghi vào hoá đơn.\nKhi kĩ thuật hoàn thành quý khách vui lòng thanh toán chi phí tại mục hoá đơn.",
                                ),
                              )
                            ],
                          ),
                          actions: <Widget>[
                            RaisedButton(
                              color: Colors.white,
                              child: Text(
                                "Đã hiểu",
                                style: TextStyle(color: Colors.redAccent),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                                Navigator.pop(context);
                              },
                            )
                          ],
                        );
                      });
                }
              });
          } else {
            Navigator.pop(context);
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Text("Vui lòng hoàn thành toàn bộ form trước"),
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Ok",
                            style: TextStyle(color: Colors.redAccent),
                          ))
                    ],
                  );
                });
          }
        },
      ),
    );
  }
}
