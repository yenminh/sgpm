import 'package:flutter/material.dart';

class PaymentPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _PaymentFul();
  }
}

class _PaymentFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PaymentState();
  }
}

class _PaymentState extends State<_PaymentFul> {
  Widget cardPayment(String type) {
    var imageLink = '';
    switch (type) {
      case 'master':
        imageLink = "images/master_card.png";
        break;
      case 'visa':
        imageLink = "images/visa.png";
        break;
      case 'paypal':
        imageLink = "images/paypal.png";
        break;
    }
    return Card(
      elevation: 4.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)), side: BorderSide(width: 1, color: Colors.black38)),
      child: InkWell(
        onTap: () {},
        child: AspectRatio(
          aspectRatio: 5 / 3,
          child: Image.asset(
            imageLink,
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Thanh toán"),
      ),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: const EdgeInsets.only(left: 16.0, right: 8.0, top: 16.0, bottom: 8.0),
                  child: cardPayment("master"),
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.only(left: 8.0, right: 16.0, top: 16.0, bottom: 8.0),
                  child: cardPayment("visa"),
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Spacer(
                flex: 1,
              ),
              Expanded(
                flex: 2,
                child: Container(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 16.0),
                  child: cardPayment("paypal"),
                ),
              ),
              Spacer(
                flex: 1,
              )
            ],
          )
        ],
      ),
    );
  }
}
