import 'package:flutter/material.dart';
import 'package:project_aparment/notification/news/news_page.dart';
import 'package:project_aparment/notification/notification/notify_page.dart';

class NotifyRootPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _NotifyFul();
  }
}

class _NotifyFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NotifyState();
}

class _NotifyState extends State<_NotifyFul> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Widget tabBar = TabBar(
      indicatorColor: Color(0xffFFFF8D),
      tabs: <Widget>[
        Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          child: Text(
            "Thông báo",
            style: TextStyle(fontSize: Theme.of(context).textTheme.title.fontSize),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          child: Text(
            "Tin tức",
            style: TextStyle(fontSize: Theme.of(context).textTheme.title.fontSize),
          ),
        )

      ],
    );
    return DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          appBar: new PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: new Container(
              color: Theme.of(context).primaryColor,
              child: new SafeArea(
                child: Column(
                  children: <Widget>[
                    Expanded(child: new Container()),
                    tabBar,
                  ],
                ),
              ),
            ),
          ) /*AppBar(
            title: Text("Thông báo"),
            centerTitle: true,
            bottom: tabBar,
          )*/
          ,
          body: TabBarView(
            children: <Widget>[NotifyPage(), NewsPage()],
          ),
        ));
  }
}
