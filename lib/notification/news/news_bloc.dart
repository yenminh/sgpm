import 'package:project_aparment/data/news.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/main.dart';
import 'dart:async';

import 'package:rxdart/rxdart.dart';

class NewsBloc extends BlocBase {
  int page = 1;
  int lastPage = 1;
  int total = 1;
  List<News> _listNews = List();
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<List<News>> _newsListBehavior = BehaviorSubject();

  Sink<List<News>> get _listNewsIn => _newsListBehavior.sink;

  Stream<List<News>> get listNewsStream => _newsListBehavior.stream;

  NewsBloc() {
    loadNews();
  }

  Future<void> loadNews() async {
    _stateIn.add(STATE_LOADING);
    Api.instance.loadNewsPage(page: 1).then((result) {
      if (result.status == 200) {
        _listNews = result.data.dataList;
        page = result.data.currentPage;
        lastPage = result.data.lastPage;
        total = result.data.total;
        _listNewsIn.add(_listNews);
        _stateIn.add(STATE_SUCCESS);
      } else {
        _stateIn.add(STATE_EMPTY);
      }
    }).catchError((error) {
      print(error.toString());
      if (_listNews == null || _listNews.isEmpty) {
        _stateIn.add(STATE_ERROR);
      }
    });
  }

  Future<void> loadNewsMore() async {
    if (page < lastPage) {
      page++;
      Api.instance.loadNewsPage(page: page).then((result) {
        if (result.status == 200) {
          _listNews.addAll(result.data.dataList);
          page = result.data.currentPage;
          lastPage = result.data.lastPage;
          total = result.data.total;
          _listNewsIn.add(_listNews);
        } else {
          print("message");
        }
      }).catchError((error) {
        print(error.toString());
      });
    }
  }

  @override
  void dispose() {
    _newsListBehavior.close();
    _stateBehaviorSubject.close();
  }

  onError() {}
}
