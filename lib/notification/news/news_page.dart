import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:project_aparment/commons/view/empty_widget.dart';
import 'package:project_aparment/commons/view/loading_widget.dart';
import 'package:project_aparment/data/news.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/notification/news/news_bloc.dart';

class NewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _NewsFul();
  }
}

class _NewsFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return _NewsState();
  }
}

class _NewsState extends State<_NewsFul> {
  NewsBloc _bloc;

  @override
  Widget build(BuildContext context) {
    _bloc = BlocProvider.of(context);
    Widget _listMain = StreamBuilder<List<News>>(
      initialData: List(),
      stream: _bloc.listNewsStream,
      builder: (context, snapshot) {
        var listData = snapshot.data;
        return RefreshIndicator(
          onRefresh: () => _bloc.loadNews(),
          child: ListView.builder(
            itemCount: snapshot.data.length * 2,
            itemBuilder: (context, i) {
              if (i.isOdd)
                return Divider(
                  height: 1,
                );
              int index = i ~/ 2;
              if (index < _bloc.total) _bloc.loadNewsMore();
              return ListTile(
                  onTap: () => Navigator.pushNamed(context, NAVIGATOR_NEWS_DETAIL, arguments: listData[index]),
                  contentPadding:
                      EdgeInsets.only(top: (index == 0) ? 16 : 8, bottom: (index == 6) ? 16 : 8, left: 16.0, right: 16.0),
                  leading: SizedBox(
                    height: 56,
                    width: 92,
                    child: CachedNetworkImage(
                      imageUrl: listData[index].photo500,
                      placeholder: (context, url) => Container(
                        color: Colors.grey,
                      ),
                      errorWidget: (context, url, error) => new Icon(Icons.error),
                    ),
                  ),
                  title: Text(
                    listData[index].tenVi,
                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                  subtitle: Text(
                    listData[index].motaVi,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontSize: 14.0, color: Colors.black54),
                  ));
            },
          ),
        );
      },
    );

    return StreamBuilder<int>(
      initialData: STATE_INIT,
      stream: _bloc.stateStream,
      builder: (context, snapshot) {
        if (snapshot.data == STATE_LOADING) {
          return LoadingWidget();
        } else if (snapshot.data == STATE_EMPTY) {
          return EmptyWidget();
        } else if(snapshot.data == STATE_ERROR){
          return EmptyWidget();
        } else {
          return _listMain;
        }
      },
    );
  }
}
