import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:project_aparment/data/news.dart';

class NewsDetails extends StatelessWidget {
  final News news;

  NewsDetails({Key key, this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(news.tenVi),
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Html(
            data: news.noiDungVi,
          ),
        ),
      ),
    );
  }
}
