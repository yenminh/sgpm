import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/data/notification.dart';
import 'package:rxdart/rxdart.dart';

class NotificationBloc extends BlocBase {
  int page = 1;
  int lastPage = 1;
  int total = 1;
  List<HomeNotification> _listNotification = List();
  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;
  //
  BehaviorSubject<List<HomeNotification>> _notificationListBehavior = BehaviorSubject();

  Sink<List<HomeNotification>> get _listNotificationIn => _notificationListBehavior.sink;

  Stream<List<HomeNotification>> get listNotificationStream => _notificationListBehavior.stream;
  //
  NotificationBloc(){
    loadListNotifications();
  }
  loadListNotifications(){
    _stateIn.add(STATE_LOADING);
    Api.instance.loadNotification(page: 1).then((result) {
      if (result.status == 200) {
        _listNotification = result.data.dataList;
        page = result.data.currentPage;
        lastPage = result.data.lastPage;
        total = result.data.total;
        _listNotificationIn.add(_listNotification);
        _stateIn.add(STATE_SUCCESS);
      } else {
        _stateIn.add(STATE_EMPTY);
      }
    }).catchError((error) {
      print(error.toString());
    });
  }
  Future<void> loadNewsMore() async {
    if (page < lastPage) {
      page++;
      Api.instance.loadNotification(page: page).then((result) {
        if (result.status == 200) {
          _listNotification.addAll(result.data.dataList);
          page = result.data.currentPage;
          lastPage = result.data.lastPage;
          total = result.data.total;
          _listNotificationIn.add(_listNotification);
        } else {
          print("message");
        }
      }).catchError((error) {
        print(error.toString());
      });
    }
  }

  @override
  void dispose() {
    _notificationListBehavior.close();
    _stateBehaviorSubject.close();
  }
}
