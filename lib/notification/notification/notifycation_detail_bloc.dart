import 'package:project_aparment/data/notification.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class NotificationDetailBloc extends BlocBase {
  HomeNotification _homeNotification;
  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<HomeNotification> _notificationBehavior = BehaviorSubject();

  Sink<HomeNotification> get _notificationIn => _notificationBehavior.sink;

  Stream<HomeNotification> get notificationStream => _notificationBehavior.stream;

  NotificationDetailBloc(int notifyId){
    _stateIn.add(STATE_LOADING);
    Api.instance.loadNotificationDetail(id: notifyId).then((result) {
      if (result.status == 200) {
        _homeNotification = result.data;
        _notificationIn.add(_homeNotification);
        _stateIn.add(STATE_SUCCESS);
      } else {
        _stateIn.add(STATE_EMPTY);
      }
    }).catchError((error) {
      print(error.toString());
    });
  }

  @override
  void dispose() {
    _stateBehaviorSubject.close();
    _notificationBehavior.close();
  }
}
