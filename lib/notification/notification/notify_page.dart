import 'package:flutter/material.dart';
import 'package:project_aparment/commons/view/empty_widget.dart';
import 'package:project_aparment/data/notification.dart';
import 'package:project_aparment/main.dart';
import 'package:project_aparment/notification/notification/notifycation_bloc.dart';

class NotifyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _NotifyFul();
  }
}

class _NotifyFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NotifyState();
  }
}

class _NotifyState extends State<_NotifyFul> {
  NotificationBloc _bloc;
  final int FROM_ADMIN = 1;
  final int FROM_ADMIN_APARTMENT = 2;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget listItem() => StreamBuilder<List<HomeNotification>>(
        initialData: List(),
        stream: _bloc.listNotificationStream,
        builder: (context, snapshot) {
          return ListView.builder(
            itemCount: snapshot.data.length * 2,
            itemBuilder: (context, i) {
              if (i.isOdd)
                return Divider(
                  height: 1,
                  indent: 72,
                );
              var index = i ~/ 2;

              return ListTile(
                leading: head(snapshot.data[index].loai ?? 3),
                onTap: () => Navigator.pushNamed(
                    context, NAVIGATOR_NOTIFICATION_DETAIL,
                    arguments: snapshot.data[index]),
                title: Text(snapshot.data[index].tenVi),
              );
            },
          );
        },
      );

  Widget head(int type) {
    if (type == FROM_ADMIN) {
      return CircleAvatar(
        child: Text("BQT", style: TextStyle(fontSize: 14, color: Colors.white)),
        backgroundColor: Colors.red[300],
      );
    } else if (type == FROM_ADMIN_APARTMENT) {
      return CircleAvatar(
        child: Text("BQL", style: TextStyle(fontSize: 14, color: Colors.white)),
        backgroundColor: Colors.red[300],
      );
    }
    return CircleAvatar(
      child: Text(
        "CD",
        style: TextStyle(fontSize: 14, color: Colors.white),
      ),
      backgroundColor: Colors.red[300],
    );
  }

  @override
  Widget build(BuildContext context) {
    _bloc = BlocProvider.of(context);
    return StreamBuilder<int>(
      initialData: STATE_INIT,
      stream: _bloc.stateStream,
      builder: (context, snapshot) {
        if (snapshot.data == STATE_LOADING) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.data == STATE_EMPTY ||
            snapshot.data == STATE_ERROR) {
          return EmptyWidget();
        } else {
          return listItem();
        }
      },
    );
  }
}
