import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:project_aparment/data/notification.dart';
import 'package:project_aparment/notification/notification/notifycation_detail_bloc.dart';

class NotificationDetail extends StatefulWidget {
//  final int _pageId;
  final HomeNotification homeNotification;

  NotificationDetail(this.homeNotification);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NotificationState();
  }
}

class _NotificationState extends State<NotificationDetail> {
  NotificationDetailBloc _bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //_bloc = NotificationDetailBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: SingleChildScrollView(
            child: Html(
              data: widget.homeNotification.noidungVi,
            ),
          )) /*StreamBuilder<int>(
        initialData: STATE_INIT,
        stream: _bloc.stateStream,
        builder: (context, snapshot) {
          if (snapshot.data == STATE_LOADING) {
            return Center(child: CircularProgressIndicator(),);
          }
          return StreamBuilder<HomeNotification>(
            stream: _bloc.notificationStream,
            builder: (context, snapshot) {
              if (snapshot != null && snapshot.data != null) {
                return Html(data: snapshot.data.noidungVi,);
              }
              return Container();
            },
          );
        },
      )*/
      ,
    );
  }
}
