import 'package:project_aparment/data/base_response.dart';
import 'package:project_aparment/data/fee_paid.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class BillPaidBloc extends BlocBase {
  BehaviorSubject<List<FeePaid>> _behaviorSubject = BehaviorSubject();

  Sink<List<FeePaid>> get _listFeeIn => _behaviorSubject.sink;

  Stream<List<FeePaid>> get listFeeStream => _behaviorSubject.stream;

  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  Api _api = Api.instance;

  BillPaidBloc() {
    loadListFee();
  }

  loadListFee() async {
    _stateIn.add(STATE_LOADING);
    BaseResponse res = await _api.getListFeePaid();
    //var listFee = res.data.map((item) => FeePaid.fromJson(Map.from(item))).toList(); //.sublist(7, 7+ 8);
    _listFeeIn.add(res.data);
    _stateIn.add(STATE_SUCCESS);
  }

  @override
  void dispose() {
    _stateBehaviorSubject.close();
    _behaviorSubject.close();
  }
}
