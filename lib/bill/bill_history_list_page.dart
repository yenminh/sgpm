import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project_aparment/bill/bill_paid_bloc.dart';
import 'package:project_aparment/data/fee_paid.dart';
import 'package:project_aparment/main.dart';

class BillHistoryListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _BillHistoryListFul();
  }
}

class _BillHistoryListFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BillHistoryListState();
  }
}

class _BillHistoryListState extends State<_BillHistoryListFul> {
  BillPaidBloc _bloc;

  Widget topView() {
    const textStyle = TextStyle(
        fontSize: 12.0, color: Colors.white, fontWeight: FontWeight.bold);
    return Container(
      padding: const EdgeInsets.all(16.0),
      color: Colors.redAccent,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              "Tháng",
              style: textStyle,
              textAlign: TextAlign.start,
            ),
          ),
          Expanded(
            child: Text(
              "Loại chi phí",
              style: textStyle,
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: Text(
              "Số tiền",
              style: textStyle,
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: Text(
              "Trạng thái",
              style: textStyle,
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: Text(
              "Hình thức",
              style: textStyle,
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: Text(
              "Tháng TT",
              style: textStyle,
              textAlign: TextAlign.end,
            ),
          )
        ],
      ),
    );
  }

  Widget itemView(int index, int itemCount, FeePaid feePaid) {
    const textStyle = TextStyle(fontSize: 12.0, color: Colors.black);
    return Container(
        padding: EdgeInsets.only(
            left: 16.0,
            right: 16.0,
            top: index == 0 ? 16.0 : 8.0,
            bottom: index == (itemCount - 1) ? 16.0 : 8.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                DateFormat("dd/MM").format(
                    DateTime.fromMicrosecondsSinceEpoch(feePaid.ngaytao)),
                style: textStyle,
                textAlign: TextAlign.start,
              ),
            ),
            Expanded(
              child: Text(
                feePaid.tenPhi,
                overflow: TextOverflow.ellipsis,
                style: textStyle,
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Text(
                NumberFormat.simpleCurrency(
                      decimalDigits: 0,
                      name: "",
                    ).format(feePaid.giatien).toString() +
                    " đ",
                style: textStyle,
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Text(
                "Đã  TT",
                style: textStyle,
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Text(
                "Tiền mặt",
                style: textStyle,
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Text(
                DateFormat("dd/MM").format(
                    DateTime.fromMicrosecondsSinceEpoch(feePaid.ngaytt)),
                style: textStyle,
                textAlign: TextAlign.end,
              ),
            )
          ],
        ));
  }

  Widget listItem() {
    return StreamBuilder<List<FeePaid>>(
      initialData: List(),
      stream: _bloc.listFeeStream,
      builder: (context, snapshot) {
        return ListView.builder(
          itemCount: snapshot.data.length,
          itemBuilder: (context, index) {
            return itemView(index, snapshot.data.length, snapshot.data[index]);
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _bloc = BlocProvider.of(context);
    return Column(
      children: <Widget>[
        topView(),
        Expanded(
          child: StreamBuilder<int>(
            initialData: STATE_INIT,
            stream: _bloc.stateStream,
            builder: (context, snapshot) {
              if (snapshot.data == STATE_LOADING)
                return Center(
                  child: CircularProgressIndicator(),
                );
              else
                return listItem();
            },
          ),
        )
      ],
    );
  }
}
