import 'package:project_aparment/data/canho.dart';
import 'package:project_aparment/data/fee.dart';
import 'package:project_aparment/data/fee_order.dart';
import 'package:project_aparment/data/repository/api.dart';
import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class BillNotPaidBloc extends BlocBase {
  BehaviorSubject<List<Fee>> _behaviorSubject = BehaviorSubject();

  Sink<List<Fee>> get _listFeeIn => _behaviorSubject.sink;

  Stream<List<Fee>> get listFeeStream => _behaviorSubject.stream;

  BehaviorSubject<int> _behaviorsummarySubject = BehaviorSubject();

  Sink<int> get _sumIn => _behaviorsummarySubject.sink;

  Stream<int> get sumStream => _behaviorsummarySubject.stream;

  //
  BehaviorSubject<int> _stateBehaviorSubject = BehaviorSubject();

  Sink<int> get _stateIn => _stateBehaviorSubject.sink;

  Stream<int> get stateStream => _stateBehaviorSubject.stream;

  //
  BehaviorSubject<Map<Fee, bool>> _feeSelectedBehaviorSubject =
      BehaviorSubject();

  Sink<Map<Fee, bool>> get _feeMapIn => _feeSelectedBehaviorSubject.sink;

  Stream<Map<Fee, bool>> get feeMapStream => _feeSelectedBehaviorSubject.stream;
  Map<Fee, bool> mapFeeSelected = Map();

//
  BehaviorSubject<Apartment> _apartmentBehaviorSubject = BehaviorSubject();

  Sink<Apartment> get _apartmentServiceIn => _apartmentBehaviorSubject.sink;

  Stream<Apartment> get apartmentStream => _apartmentBehaviorSubject.stream;

  //
  Api _api = Api.instance;

  //
  BillNotPaidBloc() {
    loadListFee();
    loadApartment();
  }

  selectFee(Fee fee) {
    (mapFeeSelected[fee] != null)
        ? mapFeeSelected[fee] = !mapFeeSelected[fee]
        : mapFeeSelected[fee] = true;
    _feeMapIn.add(mapFeeSelected);
  }

  Future<FeeOrder> getFeeOrder() async {
    List<Fee> listFee = [];
    mapFeeSelected.forEach((item, value) {
      if (value != null && value) {
        listFee.add(item);
      }
    });
    var baseData = await _api.getFee(listFee);
    return baseData.data;
  }

  clearListSelected() {}

  loadListFee() async {
    _stateIn.add(STATE_LOADING);
    var res = await _api.getListFee();
    var listFee = res.data
        .map((item) => Fee.fromJson(Map.from(item)))
        .toList(); //.sublist(7, 7+ 8);
    var sum = 0;
    listFee.forEach((item) {
      sum += item.phatsinh;
    });
    _sumIn.add(sum);
    _listFeeIn.add(listFee);
    _stateIn.add(STATE_SUCCESS);
  }

  Future<void> loadApartment() async {
    _stateIn.add(STATE_LOADING);
    _api.loadCanHo().then((result) {
      if (result != null) {
        _apartmentServiceIn.add(Apartment.fromJson(Map<String, dynamic>.from(result.data)));
      }
      _stateIn.add(STATE_SUCCESS);
    }).catchError((ex) {
      _stateIn.add(STATE_ERROR);
      print(ex.toString());
    });
  }

  @override
  void dispose() {
    _apartmentBehaviorSubject.close();
    _feeSelectedBehaviorSubject.close();
    _behaviorsummarySubject.close();
    _stateBehaviorSubject.close();
    _behaviorSubject.close();
  }
}
