import 'package:project_aparment/main.dart';
import 'package:rxdart/rxdart.dart';

class BillHistoryBloc extends BlocBase {
  int state = 0;
  BehaviorSubject<int> _stateBehavior = BehaviorSubject();

  Sink<int> get newState => _stateBehavior.sink;

  Stream<int> get newStream => _stateBehavior.stream;

  BillHistoryBloc() {
    newStream.listen((value) {
      state = value;
    });
  }

  @override
  void dispose() {
    _stateBehavior.close();
  }
}
