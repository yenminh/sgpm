import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class BillPartPage extends StatelessWidget {
  final String url;

  const BillPartPage({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("url $url");
    return Scaffold(
      appBar: AppBar(),
      body: WebView(
        javascriptMode: JavascriptMode.unrestricted,
        initialUrl: url,
      ),
    );
  }
}
