import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project_aparment/bill/bill_not_paid_bloc.dart';
import 'package:project_aparment/bill/bill_part_page.dart';
import 'package:project_aparment/data/bill_data.dart';
import 'package:project_aparment/data/canho.dart';
import 'package:project_aparment/data/fee.dart';
import 'package:project_aparment/data/fee_order.dart';
import 'package:project_aparment/main.dart';

class BillNotPaidPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _BillNotPaidFul();
  }
}

class _BillNotPaidFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BillState();
  }
}

class _BillState extends State<_BillNotPaidFul> {
  Widget topView() {
    const textStyle = TextStyle(
        fontSize: 14.0, color: Colors.white, fontWeight: FontWeight.bold);
    return Container(
      padding: const EdgeInsets.all(16.0),
      width: double.infinity,
      color: Color(0xffF6A45C),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              "Loại phí",
              style: textStyle,
              textAlign: TextAlign.start,
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              "Tháng",
              style: textStyle,
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              "Thành tiền",
              style: textStyle,
              textAlign: TextAlign.end,
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              "Chọn",
              style: textStyle,
              textAlign: TextAlign.end,
            ),
          )
        ],
      ),
    );
  }

  Fee currentFee;

  Widget item(Fee fee, index) {
    const textStyle = TextStyle(fontSize: 13.0, color: Colors.black);
    var itemCount = 12;
    return StreamBuilder<Map<Fee, bool>>(
      initialData: Map(),
      stream: _bloc.feeMapStream,
      builder: (context, snapshot) {
        return InkWell(
          onTap: () {
            _bloc.selectFee(fee);
          },
          child: Container(
            color: (snapshot.data[fee] != null && snapshot.data[fee])
                ? Colors.green[300]
                : Colors.white,
            padding: EdgeInsets.only(
                left: 16.0,
                right: 16.0,
                top: index == 0 ? 16.0 : 8.0,
                bottom: index == (itemCount - 1) ? 16.0 : 8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Container(
                    child: Text(
                      fee.tenchiphi,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: textStyle,
                      textAlign: TextAlign.start,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Text(
                      fee.ngay,
                      style: textStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    fee.phatsinh.toString(),
                    style: textStyle,
                    textAlign: TextAlign.end,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                      alignment: Alignment(1.0, 0),
                      child: AnimatedContainer(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey, width: 1.0),
                            shape: BoxShape.circle),
                        duration: Duration(milliseconds: 300),
                        height: 24,
                        width: 24,
                        child: Visibility(
                            visible: snapshot.data[fee] != null &&
                                snapshot.data[fee],
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.green,
                                      shape: BoxShape.circle),
                                ),
                              ),
                            )),
                      )),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget mainView() {
    return StreamBuilder<List<Fee>>(
      initialData: List(),
      stream: _bloc.listFeeStream,
      builder: (context, snapshot) {
        return RefreshIndicator(
          onRefresh: () async {
            await _bloc.loadListFee();
          },
          child: ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return item(snapshot.data[index], index);
              }),
        );
      },
    );
  }

  Widget bottomView() => Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(6.0), topRight: Radius.circular(6.0)),
            color: Colors.red[400]),
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: StreamBuilder<Apartment>(
                    stream: _bloc.apartmentStream,
                    builder: (context, snapshot) {
                      return AutoSizeText(
                        "Mã căn hộ: ${snapshot.data?.ma ?? ""}",
                        style: TextStyle(color: Colors.white),
                        minFontSize: 8.0,
                      );
                    },
                  ),
                ),
                InkWell(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Hướng dẫn thanh toán online"),
                            content: Text(
                                "Phần hướng dẫn thanh toán online với nội dung đang được cập nhật"),
                            actions: <Widget>[
                              FlatButton(
                                child: Text(
                                  "Ok",
                                  style: TextStyle(color: Colors.red[300]),
                                ),
                                onPressed: () => Navigator.pop(context),
                              )
                            ],
                          );
                        });
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.help,
                        color: Colors.white,
                        size: 16.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          "Hướng dẫn thanh toán online",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            Divider(
              color: Colors.white,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Text(
                              "Tổng TT: ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: StreamBuilder<int>(
                              initialData: 0,
                              stream: _bloc.sumStream,
                              builder: (context, snapshot) {
                                return Text(
                                  NumberFormat.simpleCurrency(
                                        decimalDigits: 0,
                                        name: "",
                                      ).format(snapshot.data).toString() +
                                      " đ",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                );
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Text(
                              "Tổng cộng: ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Container(
                              alignment: Alignment(-1.0, 0.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.white, width: 1.0),
                                    borderRadius: BorderRadius.circular(2.0)),
                                margin: EdgeInsets.only(top: 8.0),
                                padding: const EdgeInsets.only(
                                    top: 4.0,
                                    bottom: 4.0,
                                    left: 16.0,
                                    right: 16.0),
                                child: StreamBuilder<Map<Fee, bool>>(
                                  initialData: Map(),
                                  stream: _bloc.feeMapStream,
                                  builder: (context, snapshot) {
                                    var sum = 0;
                                    var text =
                                        snapshot.data.forEach((key, value) {
                                      if (value) {
                                        sum = sum + key.phatsinh;
                                      }
                                    });
                                    return Text(
                                      NumberFormat.simpleCurrency(
                                            decimalDigits: 0,
                                            name: "",
                                          ).format(sum).toString() +
                                          " đ",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    );
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: RaisedButton(
                      padding: const EdgeInsets.all(16),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4.0))),
                      splashColor: Colors.white70,
                      onPressed: () async {
                        showDialog(
                            context: context,
                            builder: (context) => Center(
                                  child: CircularProgressIndicator(),
                                ));

                        if (_bloc.mapFeeSelected != null) {
                          FeeOrder fee = await _bloc.getFeeOrder();
                          Navigator.pop(context);
                          if (fee != null) {
                            var sum = 0;
                            _bloc.mapFeeSelected.forEach((key, value) {
                              if (value) {
                                sum = sum + key.phatsinh;
                              }
                            });
                            var bill = BillData(
                                transAmount: sum,
                                orderId: fee.orderId.toString(),
                                billCode: fee.orderId.toString(),
                                description: "Thanh toan chi phi dich vu");
                            //_launchURL(bill);
                            print("push tt");
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BillPartPage(
                                          url: bill.getUrlCTT(),
                                        )));
                          }
                        } else {
                          Navigator.pop(context);
                        }
                      },
                      child: Text(
                        "Thanh toán",
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      color: Color(0xffF7941D),
                    ))
              ],
            )
          ],
        ),
      );
  BillNotPaidBloc _bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _bloc = BlocProvider.of(context);
    return Column(
      children: <Widget>[
        topView(),
        Expanded(
          child: StreamBuilder<int>(
            initialData: STATE_INIT,
            stream: _bloc.stateStream,
            builder: (context, snapshot) {
              if (snapshot.data == STATE_LOADING)
                return Center(
                  child: CircularProgressIndicator(),
                );
              return mainView();
            },
          ),
        ),
        bottomView()
      ],
    );
  }

  _launchURL(BillData bill) async {
    var url = bill.getUrlCTT();
    print(url);
    //launch(url);
    /*if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }*/
  }
}
