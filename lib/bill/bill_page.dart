import 'package:flutter/material.dart';
import 'package:project_aparment/bill/bill_history_list_page.dart';
import 'package:project_aparment/bill/bill_not_paid.dart';

class BillPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _BillFul();
  }
}

class _BillFul extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BillState();
}

class _BillState extends State<_BillFul> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget tabBar = TabBar(
      indicatorColor: Colors.red,
      tabs: <Widget>[
        Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          child: Text(
            "Chưa thanh toán",
          ),
        ),
        Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          child: Text(
            "Lịch sử thanh toán",
          ),
        )
      ],
    );
    return DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: Scaffold(
              appBar: new PreferredSize(
                preferredSize: Size.fromHeight(kToolbarHeight),
                child: new Container(
                  color: Theme.of(context).primaryColor,
                  child: new SafeArea(
                    child: Column(
                      children: <Widget>[
                        Expanded(child: new Container()),
                        tabBar,
                      ],
                    ),
                  ),
                ),
              ),
              body: TabBarView(
                children: <Widget>[BillNotPaidPage(), BillHistoryListPage()],
              ),
            )) /*Scaffold(
      appBar: AppBar(
        leading: Container(),
        centerTitle: true,
        title: Text(
          "Chưa thanh toán",
          style: TextStyle(fontSize: Theme.of(context).textTheme.title.fontSize),
        ),
      ),
      body: BillNotPaidPage(),
    )*/
        ;
  }
}
